package com.timely.springserver.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.timely.springserver.model.Task;


@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskReposioryTest {
	
    @Autowired
    private TestEntityManager entityManager;
    
    @Autowired
    private TaskRepository taskRepository;
    
	@Test
	public void should_find_no_tasks_if_repository_is_empty() {
		Iterable<Task> tasks = taskRepository.findAll();
 
		assertThat(tasks).isEmpty();
	}
	
	@Test
	public void should_delete_task() {
		Task task = new Task();
		task.setDescription("test");
		task.setStartDate(Instant.now());
		task.setEndDate(Instant.now());
		
		entityManager.persist(task);
 
		taskRepository.deleteById(task.getId());
 
		assertThat(taskRepository.findAll()).isEmpty();
	}
 
}
