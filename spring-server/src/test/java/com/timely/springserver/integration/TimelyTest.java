package com.timely.springserver.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.timely.springserver.payload.AuthResponse;
import com.timely.springserver.payload.LoginRequest;

public class TimelyTest {
	
    @Autowired
	protected TestRestTemplate restTemplate;
	
	protected String login(String email, String password) {
    	LoginRequest loginRequest = new LoginRequest();
    	loginRequest.setEmail(email);
    	loginRequest.setPassword(password);
    	
        ResponseEntity<AuthResponse> responseEntity =
                restTemplate.postForEntity("/auth/login", loginRequest, AuthResponse.class);
        
        return responseEntity.getBody().getAccessToken();
    }
    
    protected HttpHeaders createHeader(String email, String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + login(email, password));
        return headers;
    }

}
