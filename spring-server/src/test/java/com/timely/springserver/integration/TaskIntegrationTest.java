package com.timely.springserver.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.Instant;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.timely.springserver.model.Task;
import com.timely.springserver.model.User;
import com.timely.springserver.payload.ApiResponse;
import com.timely.springserver.payload.TaskRequest;
import com.timely.springserver.repository.TaskRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TaskIntegrationTest extends TimelyTest {
    
	@Autowired
	private TaskRepository taskRepository;
	   
    @Test
    public void test1_getTasks(){
    	final ResponseEntity<User> responseEntity = restTemplate.exchange("/user/me", HttpMethod.GET
    			, new HttpEntity<>(createHeader("admin@admin.pt", "password")), User.class);
        
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
    
    @Test
    public void test2_createTask() {
    	taskRepository.deleteAll();
    	assertEquals(0, taskRepository.findAll().size());
    	
    	TaskRequest taskRequest = new TaskRequest();
    	taskRequest.setDescription("tes");
    	taskRequest.setStartDate(Instant.now());
    	taskRequest.setEndDate(Instant.now().minusSeconds(120));
    	
    	final ResponseEntity<ApiResponse> responseEntity = restTemplate.exchange("/task", HttpMethod.POST
    			, new HttpEntity<>(taskRequest, createHeader("admin@admin.pt", "password")), ApiResponse.class);
        	
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(1, taskRepository.findAll().size());
    }
    
    @Test
    public void test3_getTask() {
		test2_createTask();
		List<Task> tasks = taskRepository.findAll();
    	
		Task task = tasks.get(tasks.size() - 1);
		
    	final ResponseEntity<Task> responseEntity = restTemplate.exchange("/task/"+ task.getId(), HttpMethod.GET
    			, new HttpEntity<>(createHeader("admin@admin.pt", "password")), Task.class);
        	
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(task.getId(), responseEntity.getBody().getId());
    }
    
    @Test
    public void test4_updateTask() {
		test2_createTask();
		List<Task> tasks = taskRepository.findAll();
		Task task = tasks.get(tasks.size() - 1);
		Instant endDate = Instant.now();
		
    	TaskRequest taskRequest = new TaskRequest();
    	taskRequest.setDescription("test2");
    	taskRequest.setStartDate(endDate.minusSeconds(120));
    	taskRequest.setEndDate(endDate);
		
    	final ResponseEntity<Task> responseEntity = restTemplate.exchange("/task/"+ task.getId(), HttpMethod.PUT
    			, new HttpEntity<>(taskRequest, createHeader("admin@admin.pt", "password")), Task.class);
        	
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        
        Task taskAfterUpdate = taskRepository.findById(task.getId()).orElse(null);
        assertNotNull(taskAfterUpdate);
        assertEquals("test2", taskAfterUpdate.getDescription());
        assertEquals(Long.valueOf(2), taskAfterUpdate.getDurationInMinutes());
    }
    
    @Test
    public void test6_deleteTask() {
    	taskRepository.deleteAll();
    	test2_createTask();
    	List<Task> tasks = taskRepository.findAll();
		Task task = tasks.get(tasks.size() - 1);
		
		final ResponseEntity<ApiResponse> responseEntity = restTemplate.exchange("/task/"+ task.getId(), HttpMethod.DELETE
    			, new HttpEntity<>(createHeader("admin@admin.pt", "password")), ApiResponse.class);
		
		 assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		 assertEquals(0, taskRepository.findAll().size());
    }
}
