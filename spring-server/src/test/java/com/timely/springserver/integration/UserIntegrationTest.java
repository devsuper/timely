package com.timely.springserver.integration;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.timely.springserver.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserIntegrationTest extends TimelyTest {
	   
	 @Test
	 public void test_getMe(){
	 	final ResponseEntity<User> responseEntity = restTemplate.exchange("/user/me", HttpMethod.GET
	 			, new HttpEntity<>(createHeader("admin@admin.pt", "password")), User.class);
	     
	     assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	 }
	 
	 @Test
	 public void test_getMe_Notautorized(){
	 	final ResponseEntity<User> responseEntity = restTemplate.exchange("/user/me", HttpMethod.GET
	 			, null, User.class);
	     
	     assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
	 }
}
