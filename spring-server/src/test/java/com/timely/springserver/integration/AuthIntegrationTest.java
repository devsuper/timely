package com.timely.springserver.integration;

import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.timely.springserver.model.VerificationToken;
import com.timely.springserver.payload.ApiResponse;
import com.timely.springserver.payload.AuthResponse;
import com.timely.springserver.payload.LoginRequest;
import com.timely.springserver.payload.SignUpRequest;
import com.timely.springserver.repository.VerificationTokenRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AuthIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;
    
    @Autowired
    private VerificationTokenRepository verificationTokenRepository;
    
    
    @Test
    public void test1_signup_user() {
    	SignUpRequest singupRequest = new SignUpRequest();
    	singupRequest.setName("Tester");
    	singupRequest.setEmail("email@email.pt");
    	singupRequest.setPassword("password");
    	
        ResponseEntity<ApiResponse> responseEntity =
            restTemplate.postForEntity("/auth/signup", singupRequest, ApiResponse.class);
        
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
    
    @Test
    public void test2_loginFailure_emailVerification(){
    	LoginRequest loginRequest = new LoginRequest();
    	loginRequest.setEmail("email@email.pt");
    	loginRequest.setPassword("password");
    	
        ResponseEntity<ApiResponse> responseEntity =
                restTemplate.postForEntity("/auth/login", loginRequest, ApiResponse.class);
       
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }
    
    @Test
    public void test3_registrationConfirm(){
    	VerificationToken token = verificationTokenRepository.findAll().get(0);
    	
        ResponseEntity<ApiResponse> responseEntity =
                restTemplate.getForEntity("/auth/regitrationConfirm?token="+token.getToken(), ApiResponse.class); 
       
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
    
    @Test
    public void test4_loginConfirm(){
    	LoginRequest loginRequest = new LoginRequest();
    	loginRequest.setEmail("email@email.pt");
    	loginRequest.setPassword("password");
    	
        ResponseEntity<AuthResponse> responseEntity =
                restTemplate.postForEntity("/auth/login", loginRequest, AuthResponse.class);
       
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
    
}
