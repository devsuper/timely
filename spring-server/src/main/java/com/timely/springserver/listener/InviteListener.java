package com.timely.springserver.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.timely.springserver.email.OnInviteEvent;

@Component
public class InviteListener implements
  ApplicationListener<OnInviteEvent> {
 
    @Autowired
    private JavaMailSender mailSender;
 
    @Override
    public void onApplicationEvent(OnInviteEvent event) {
        this.confirmRegistration(event);
    }
 
    private void confirmRegistration(OnInviteEvent event) {
        String recipientAddress = event.getEmail();
     
        String subject = "Inivte to join timely";
        String confirmationUrl  = event.getAppUrl() + "/singup";
           
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText("Wea are pleased to invite you to join timely." + " rn " + confirmationUrl);
        
        mailSender.send(email);
    }
}
