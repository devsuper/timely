package com.timely.springserver.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import com.timely.springserver.email.OnNewPassword;

@Component
public class NewPasswordListener implements
  ApplicationListener<OnNewPassword> {
  
    @Autowired
    private JavaMailSender mailSender;
 
    @Override
    public void onApplicationEvent(OnNewPassword event) {
        this.confirmRegistration(event);
    }
 
    private void confirmRegistration(OnNewPassword event) {
        String password = event.getPassword();
         
        String recipientAddress = event.getEmail();
        String subject = "New Password";
           
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText("Please use this password : " + password);
        
        mailSender.send(email);
    }
}