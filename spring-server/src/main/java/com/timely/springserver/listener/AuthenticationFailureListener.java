package com.timely.springserver.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.timely.springserver.service.UserService;

@Component
public class AuthenticationFailureListener 
  implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
 
    @Autowired
    private UserService userService;
 
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
    	 String email = (String) event.getAuthentication().getPrincipal();
         userService.authenticationFailure(email); 
    }
}

