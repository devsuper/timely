package com.timely.springserver.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.timely.springserver.model.Note;

public interface NoteRepository extends JpaRepository<Note, Long> {
	 Optional<Note> findByTaskIdAndId(Long taskId, Long id);
}
