package com.timely.springserver.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.timely.springserver.model.Role;
import com.timely.springserver.model.RoleName;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
