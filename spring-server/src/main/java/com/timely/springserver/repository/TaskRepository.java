package com.timely.springserver.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.timely.springserver.model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {
   Optional<Task> findByUserIdAndId(Long userId, Long id);
   List<Task> findByUserId(Long userId);
   Page<Task> findByUserId(Long id, Pageable pagebale);
   Page<Task> findByUserIdAndStartDateBetween(Long id, Instant startDate, Instant endDate, Pageable pagebale);
}
