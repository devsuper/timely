package com.timely.springserver.controller;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.timely.springserver.model.Task;
import com.timely.springserver.payload.ApiResponse;
import com.timely.springserver.payload.TaskRequest;
import com.timely.springserver.payload.TaskResponse;
import com.timely.springserver.security.CurrentUser;
import com.timely.springserver.security.UserPrincipal;
import com.timely.springserver.service.TaskService;

@RestController
public class TaskController {

	@Autowired
	private TaskService taskService;

	@PostMapping("/task")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse createTask(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody TaskRequest taskRequest) {
		taskService.createTask(userPrincipal.getId(), taskRequest);
		
		return new ApiResponse(true, "Task created with success");
	}
	
	@PostMapping("/user/{userId}/task")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ApiResponse createTaskForUser(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "userId") Long userId,
			@Valid @RequestBody TaskRequest taskRequest) {
		
		taskService.createTaskForUser(userPrincipal.getId(), userId, taskRequest);
		
		return new ApiResponse(true, "Task created with success");
	}
	
	@PutMapping("/task/{taskId}")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse updateTask(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "taskId") Long taskId, 
			@Valid @RequestBody TaskRequest taskRequest) {
		taskService.updateTask(userPrincipal.getId(), taskId, taskRequest);
		
		return new ApiResponse(true, "Task updated with success");
	}
	
	@PutMapping("/user/{userId}/task/{taskId}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ApiResponse updateTaskForUser(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "taskId") Long taskId, 
			@Valid @RequestBody TaskRequest taskRequest) {
		taskService.updateTaskForUser(userPrincipal.getId(), userId, taskId, taskRequest);
		
		return new ApiResponse(true, "Task updated with success");
	}
	
	@GetMapping("/task")
    @PreAuthorize("isAuthenticated()")
	public Page<TaskResponse> getTasks(@CurrentUser UserPrincipal userPrincipal,
			@RequestParam(value = "startDate", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
			@RequestParam(value = "endDate", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate, 
			Pageable pageable) {
		Instant startDateInstant = startDate != null ? startDate.atZone(ZoneId.of("Europe/Paris")).toInstant() : null;
		Instant endDateInstant = endDate != null ? endDate.atZone(ZoneId.of("Europe/Paris")).toInstant() : null;
		
		return taskService.getTasks(userPrincipal.getId(), startDateInstant, endDateInstant, pageable);
	}
	
	@GetMapping("/user/{userId}/task")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public Page<TaskResponse> getTasksForUser(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "userId") Long userId,
			@RequestParam(value = "startDate", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
			@RequestParam(value = "endDate", required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate, 
			Pageable pageable) {
		
		Instant startDateInstant = startDate != null ? startDate.atZone(ZoneId.of("Europe/Paris")).toInstant() : null;
		Instant endDateInstant = endDate != null ? endDate.atZone(ZoneId.of("Europe/Paris")).toInstant() : null;
		
		return taskService.getTasksForUsers(userPrincipal.getId(), userId, startDateInstant, endDateInstant, pageable);
	}
	
	@GetMapping("/task/{taskId}")
	@PreAuthorize("isAuthenticated()")
	public Task getTask(@CurrentUser UserPrincipal userPrincipal, @PathVariable(value = "taskId") Long taskId) {
		return taskService.getTask(userPrincipal.getId(), taskId);
	}
	
	@GetMapping("/user/{userId}/task/{taskId}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public Task getTaskForUser(@CurrentUser UserPrincipal userPrincipal, @PathVariable(value = "userId") Long userId, @PathVariable(value = "taskId") Long taskId) {
		return taskService.getTaskForUser(userPrincipal.getId(), userId, taskId);
	}
	
	@DeleteMapping("/task/{taskId}")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse deleteTask(@CurrentUser UserPrincipal userPrincipal, @PathVariable(value = "taskId") Long taskId) {
		taskService.deleteTask(userPrincipal.getId(), taskId);
		
		return new ApiResponse(true, "Task Deleted Successfully");
	}
	
	@DeleteMapping("/user/{userId}/task/{taskId}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ApiResponse deleteTaskForUser(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "userId") Long userId, 
			@PathVariable(value = "taskId") Long taskId) {
		taskService.deleteTaskForUser(userPrincipal.getId(), userId, taskId);
		
		return new ApiResponse(true, "Task Deleted Successfully");
	}
}
