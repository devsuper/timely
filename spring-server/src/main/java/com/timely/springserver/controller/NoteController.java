package com.timely.springserver.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.timely.springserver.payload.ApiResponse;
import com.timely.springserver.payload.NoteRequest;
import com.timely.springserver.security.CurrentUser;
import com.timely.springserver.security.UserPrincipal;
import com.timely.springserver.service.NoteService;

public class NoteController {
	
	@Autowired
	private NoteService noteService;

	@PostMapping("/task/{taskId}/note")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse createNote(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "taskId") Long taskId,
			@Valid @RequestBody NoteRequest noteRequest) {
		noteService.createNote(userPrincipal.getId(), taskId, noteRequest);
		
		return new ApiResponse(true, "Note created with success");
	}
	
	@DeleteMapping("/task/{taskId}/note/{noteId}")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse deleteNote(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "taskId") Long taskId,
			@PathVariable(value = "noteId") Long noteId) {
		noteService.deleteNote(userPrincipal.getId(), taskId, noteId);
		
		return new ApiResponse(true, "Note deleted with success");
	}
	
	@PostMapping("/user/{userId}/task/{taskId}/note")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ApiResponse createNoteForUser(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "taskId") Long taskId,
			@Valid @RequestBody NoteRequest noteRequest) {
		noteService.createNote(userId, taskId, noteRequest);
		
		return new ApiResponse(true, "Note created with success");
	}
	
	@DeleteMapping("/user/{userId}/task/{taskId}/note/{noteId}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ApiResponse deleteNoteForUser(@CurrentUser UserPrincipal userPrincipal, 
			@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "taskId") Long taskId,
			@PathVariable(value = "noteId") Long noteId) {
		noteService.deleteNote(userId, taskId, noteId);
		
		return new ApiResponse(true, "Note deleted with success");
	}
}
