
package com.timely.springserver.controller;

import java.util.Calendar;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.timely.springserver.email.OnRegistrationCompleteEvent;
import com.timely.springserver.exception.BadRequestException;
import com.timely.springserver.model.AuthProvider;
import com.timely.springserver.model.Role;
import com.timely.springserver.model.RoleName;
import com.timely.springserver.model.User;
import com.timely.springserver.model.VerificationToken;
import com.timely.springserver.payload.ApiResponse;
import com.timely.springserver.payload.AuthResponse;
import com.timely.springserver.payload.LoginRequest;
import com.timely.springserver.payload.SignUpRequest;
import com.timely.springserver.repository.RoleRepository;
import com.timely.springserver.repository.UserRepository;
import com.timely.springserver.security.TokenProvider;
import com.timely.springserver.service.UserService;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private UserService userService;

	@Autowired
	private RoleRepository roleRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenProvider tokenProvider;
    
    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @PostMapping("/login")
    public AuthResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    	if(userRepository.existsByEmail(loginRequest.getEmail())) {
    		User user = userRepository.findByEmail(loginRequest.getEmail()).get();
            if (!user.getEmailVerified()) {
            	throw new BadRequestException("Email is not verified");
            } else if  (user.getLoginAttemps() >= 3) {
	        	throw new BadRequestException("Exceed max number of attemps");
	        } 
    	}
    	
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        
        Set<String> roles = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
		Optional<String> role = roles.stream().findFirst();
        
        return new AuthResponse(token, role.isPresent() ? role.get() : "");
    }

    @PostMapping("/signup")
    public ApiResponse registerUser(HttpServletRequest request, @Valid @RequestBody SignUpRequest signUpRequest) {
        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new BadRequestException("Email address already in use.");
        }
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new BadRequestException("User Role not set."));

//        byte[] image = null;
//        try {
//        	File file = new File("src/main/resources/images/avatar.png");
//        	image = Files.readAllBytes(file.toPath());
//        } 
        // Creating user's account
        User user = new User();
        user.setName(signUpRequest.getName());
        user.setEmail(signUpRequest.getEmail());
        user.setProvider(AuthProvider.local);
        user.setRole(userRole);
        user.setImage(new byte[0]);
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

        User result = userRepository.save(user);
        
        try {
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(result, getAppUrl(request)));
        } catch (Exception me) {
        	 return new ApiResponse(true, "User registered unsucces" + me.getMessage());
        }

        return new ApiResponse(true, "User registered successfully");
    }
    	
    @GetMapping("/regitrationConfirm")
    public ApiResponse regitrationConfirm(@RequestParam("token") String token) {
    	VerificationToken verificationToken = userService.getVerificationToken(token);
        if (verificationToken == null) {
            return new ApiResponse(true, "Invalid token");
        }
         
        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
        	return new ApiResponse(true, "Experied token");
        } 
         
        user.setEmailVerified(true); 
        
        userService.saveRegisteredUser(user); 
        
        return new ApiResponse(true, "Correct token");
    }
    
    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

}
