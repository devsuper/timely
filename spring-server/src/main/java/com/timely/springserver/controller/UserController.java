package com.timely.springserver.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.timely.springserver.email.OnInviteEvent;
import com.timely.springserver.email.OnNewPassword;
import com.timely.springserver.email.OnRegistrationCompleteEvent;
import com.timely.springserver.exception.BadRequestException;
import com.timely.springserver.model.User;
import com.timely.springserver.payload.ApiResponse;
import com.timely.springserver.payload.PasswordRequest;
import com.timely.springserver.payload.ProfileRequest;
import com.timely.springserver.payload.UserRequest;
import com.timely.springserver.repository.UserRepository;
import com.timely.springserver.security.CurrentUser;
import com.timely.springserver.security.UserPrincipal;
import com.timely.springserver.service.UserService;
import com.timely.springserver.util.Utils;

@RestController
public class UserController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    
    @GetMapping("/user/me")
    @PreAuthorize("isAuthenticated()")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
    	return userService.getUser(userPrincipal.getId());
    }
    
	@PutMapping("/profile")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse updateProfile(@CurrentUser UserPrincipal userPrincipal,
			@Valid @RequestBody ProfileRequest profileRequest) {
		
		userService.updateProfile(userPrincipal.getId(), profileRequest);
		
		return new ApiResponse(true, "User Updated Successfully");
	}
    
    @GetMapping("/user/me/avatar")
    @PreAuthorize("isAuthenticated()")
    public byte[] getAvatar(@CurrentUser UserPrincipal userPrincipal) {
    	User user = userService.getUser(userPrincipal.getId());
    	return user.getImage();
    }
    
    @PostMapping("/user/avatar/upload/{userId}")
    @PreAuthorize("isAuthenticated()")
    public ApiResponse uploadFile(@CurrentUser UserPrincipal userPrincipal, 
    		@PathVariable(value = "userId") Long userId,
    		@RequestParam("file") MultipartFile file) {
    	
    	userService.uploadAvatar(userPrincipal.getId(), userId, file);

        return new ApiResponse(true, "Avatar updated ");
    }
    
	@PostMapping("/user")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public User createUser(HttpServletRequest request, @CurrentUser UserPrincipal userPrincipal, 
			@Valid @RequestBody UserRequest userRequest) {
		String password = Utils.generateRandomPassword();
		User user = userService.createUser(userPrincipal.getId(), userRequest, password);
		
        try {
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, getAppUrl(request)));
            eventPublisher.publishEvent(new OnNewPassword(password, user.getEmail()));
        } catch (Exception me) {
        	throw new BadRequestException("Failed to send confirmation email");
        } 
		
		return user;
	}
	
	@PutMapping("/user/{userId}")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public ApiResponse updateUser(@CurrentUser UserPrincipal userPrincipal,
			@Valid @RequestBody UserRequest userRequest, 
			@PathVariable(value = "userId") Long userId) {
		
		userService.updateUser(userPrincipal.getId(), userId, userRequest);
		
		return new ApiResponse(true, "User Updated Successfully");
	}
	
	@DeleteMapping("/user/{userId}")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public ApiResponse deleteUser(@CurrentUser UserPrincipal userPrincipal, @PathVariable(value = "userId") Long userId) {
		userService.deleteUser(userPrincipal.getId(), userId);

		return new ApiResponse(true, "User Deleted Successfully");
	}
	

	@GetMapping("/user")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public Page<User> getUsers(@CurrentUser UserPrincipal userPrincipal, Pageable pageable) {
		return userService.getUsers(userPrincipal.getId(), pageable);
	}
	
	@GetMapping("/user/{userId}")
	@PreAuthorize("hasAnyRole('ADMIN', 'MANAGER')")
	public User getUser(@CurrentUser UserPrincipal userPrincipal, @PathVariable(value = "userId") Long userId) {
		return userService.getUser(userPrincipal.getId(), userId);
	}
	
	@GetMapping("/user/invite")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ApiResponse inviteUser(HttpServletRequest request, @CurrentUser UserPrincipal userPrincipal, 
			@RequestParam(value = "email", required=true) @Valid @Email String email) {
		
        if(userRepository.existsByEmail(email)) {
            throw new BadRequestException("Email address already in use.");
        }
		
        try {
            eventPublisher.publishEvent(new OnInviteEvent("http://localhost:3000/", email));
        } catch (Exception me) {
        	return new ApiResponse(true, "Failed to send confirmation email");
        }
        
        return new ApiResponse(true, "Invite sent with success");
	}

    private String getAppUrl(HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
	
	@GetMapping("/user/reset/{userId}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ApiResponse resetPassword(@CurrentUser UserPrincipal userPrincipal, @PathVariable(value = "userId") Long userId) {
		String password = Utils.generateRandomPassword();
		User user = userService.resetUser(userId, password);
		
        try {
            eventPublisher.publishEvent(new OnNewPassword(password, user.getEmail()));
        } catch (Exception me) {
        	throw new BadRequestException("Failed to send new password");
        } 
		
		return new ApiResponse(true, "User reseted with success");
	}
	
	@PutMapping("/user/password/")
	@PreAuthorize("isAuthenticated()")
	public ApiResponse savePassword(@CurrentUser UserPrincipal userPrincipal, @Valid @RequestBody PasswordRequest passwordRequest) {
		userService.changePassword(userPrincipal.getId(), passwordRequest);
		return new ApiResponse(true, "User reseted with success");
	}	
}
