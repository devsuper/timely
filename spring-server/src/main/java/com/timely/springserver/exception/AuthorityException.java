package com.timely.springserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AuthorityException extends RuntimeException {

	private static final long serialVersionUID = -1991669863456008012L;

	public AuthorityException(Long creatorId, Long userId) {
        super(String.format("%d user don't have permissions over %d", creatorId, userId));
	}
	
	public AuthorityException(Long creatorId, String roleName) {
        super(String.format("%d user don't have permissions over to create user with role %s", creatorId, roleName));
	}
}
