package com.timely.springserver.email;

import org.springframework.context.ApplicationEvent;

public class OnInviteEvent extends ApplicationEvent {
	
	private static final long serialVersionUID = -8152990987027537350L;
	
	private String appUrl;
	private String email;
	
	public OnInviteEvent(String appUrl, String email) {
		super(email);
		this.appUrl = appUrl;
		this.email = email;
	}
	
	public String getAppUrl() {
		return appUrl;
	}
	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
