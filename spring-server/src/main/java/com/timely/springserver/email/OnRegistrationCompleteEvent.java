package com.timely.springserver.email;

import org.springframework.context.ApplicationEvent;

import com.timely.springserver.model.User;

public class OnRegistrationCompleteEvent extends ApplicationEvent {

	private static final long serialVersionUID = -4901330472669286696L;

	private String appUrl;
    private User user;
 
    public OnRegistrationCompleteEvent(
      User user, String appUrl) {
        super(user);
         
        this.setUser(user);
        this.setAppUrl(appUrl);
    }

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}

