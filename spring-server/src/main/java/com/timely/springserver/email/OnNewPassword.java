package com.timely.springserver.email;

import org.springframework.context.ApplicationEvent;

public class OnNewPassword extends ApplicationEvent {
	
	private static final long serialVersionUID = -8152990987027537350L;
	
	private String password;
	private String email;
	
	public OnNewPassword(String password, String email) {
		super(email);
		this.password = password;
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
