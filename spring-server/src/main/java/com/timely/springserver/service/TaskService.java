package com.timely.springserver.service;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.timely.springserver.exception.AuthorityException;
import com.timely.springserver.exception.BadRequestException;
import com.timely.springserver.exception.ResourceNotFoundException;
import com.timely.springserver.model.Task;
import com.timely.springserver.model.User;
import com.timely.springserver.payload.TaskRequest;
import com.timely.springserver.payload.TaskResponse;
import com.timely.springserver.repository.TaskRepository;
import com.timely.springserver.repository.UserRepository;

@Service
public class TaskService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	public Task getTask(Long userId, Long taskId) {
		return taskRepository.findByUserIdAndId(userId, taskId).orElseThrow(() ->  new ResourceNotFoundException("Task", "Id", taskId));
	}
	
	public Task getTaskForUser(Long creatorId, Long userId, Long taskId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", userId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		return getTask(userId, taskId);
	}

	public Page<TaskResponse> getTasks(Long userId, Instant startDate, Instant endDate, Pageable pageable) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		Page<Task> tasks = (startDate == null || endDate == null) ?
				taskRepository.findByUserId(userId, pageable) :
					taskRepository.findByUserIdAndStartDateBetween(userId, startDate, endDate, pageable);

		Map<String, Boolean> hoursPerDayValide = checkHoursPerDay(user.getTasks(), user.getPreferredWorkingHourPerDay());
	
		return tasks.map(task -> convertToResponse(task, hoursPerDayValide));
	}
	
	private Map<String, Boolean> checkHoursPerDay(List<Task> tasks, Long maxHoursPerDay) {
		Map<String, List<Task>> tasksPerDay = tasks.stream().collect(Collectors.groupingBy(Task::getDayString));
		
		final Long maxHours = maxHoursPerDay != null ? maxHoursPerDay : Long.valueOf(0);
		
		return tasksPerDay.
				entrySet().
				stream().
				collect(Collectors.toMap(Map.Entry::getKey, e -> {
					Long sumOfMinutesThisDay = e.getValue().stream().mapToLong(Task::getDurationInMinutes).sum();
					return sumOfMinutesThisDay > maxHours;
				}));
	}
	
	public TaskResponse convertToResponse(Task task, Map<String, Boolean> hoursPerDayValide) {
		TaskResponse taskResponse = new TaskResponse();
		taskResponse.setId(task.getId());
		taskResponse.setDescription(task.getDescription());
		taskResponse.setStartDate(task.getStartDate());
		taskResponse.setEndDate(task.getEndDate());
		taskResponse.setNotes(task.getNotes());
		taskResponse.setExceedDailyHour(hoursPerDayValide.get(task.getDayString()));
		taskResponse.setDuration(task.getDurationInMinutes());
		
		return taskResponse;
	}
	
	public Page<TaskResponse> getTasksForUsers(Long creatorId, Long userId, Instant startDate, Instant endDate, Pageable pageable) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));	
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", userId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		return getTasks(userId, startDate, endDate, pageable);
	}
	
	public void createTask(Long userId, TaskRequest request) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		validateTask(userId, request);
		
		Task task = new Task();
		task.setDescription(request.getDescription());
		task.setStartDate(request.getStartDate());
		task.setEndDate(request.getEndDate());
		task.setUser(user);

		taskRepository.save(task);
	}
	
	public void validateTask(Long userId, TaskRequest request) {
		List<Task> tasks = taskRepository.findByUserId(userId);
		
		tasks.forEach(task -> {
			if (request.getEndDate() == null && task.getEndDate() == null) {
				throw new BadRequestException("You already have an on going task!");
			} 
			if (task.getEndDate() != null) {	
				if (request.getStartDate().isAfter(task.getStartDate()) &&
						request.getStartDate().isBefore(task.getEndDate())) {
					throw new BadRequestException("Start date is within another task");
				}
				if (request.getEndDate() != null 
						&& (request.getEndDate().isAfter(task.getStartDate()) && request.getEndDate().isBefore(task.getEndDate()))){
					throw new BadRequestException("End date is within another task");
				}
				
				if (request.getEndDate() != null 
						&& (request.getStartDate().isBefore(task.getStartDate()) && request.getEndDate().isAfter(task.getEndDate()))){
					throw new BadRequestException("Start date and End date are within another task");
				}
			}
		});
	}
	
	public void createTaskForUser(Long creatorId, Long userId, TaskRequest request) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", userId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		createTask(userId, request);
	}
	
	public void updateTask(Long userId, Long taskId, TaskRequest request) {
		Task task = taskRepository.findByUserIdAndId(userId, taskId).orElseThrow(() ->  new ResourceNotFoundException("Task", "Id", taskId));
		
		validateTask(userId, request);
		
		task.setDescription(request.getDescription());
		task.setStartDate(request.getStartDate());
		task.setEndDate(request.getEndDate());
		taskRepository.save(task);
	}
	
	public void updateTaskForUser(Long creatorId, Long userId, Long taskId, TaskRequest request) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", userId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		updateTask(userId, taskId, request);
	}
	
	public void deleteTask(Long userId, Long taskId) {
		Task task = taskRepository.findByUserIdAndId(userId, taskId).orElseThrow(() ->  new ResourceNotFoundException("Task", "Id", taskId));
		
		taskRepository.deleteById(task.getId());
	}
	
	public void deleteTaskForUser(Long creatorId, Long userId, Long taskId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", userId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		deleteTask(userId, taskId);
	}
}
