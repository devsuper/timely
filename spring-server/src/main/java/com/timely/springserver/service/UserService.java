package com.timely.springserver.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.timely.springserver.exception.AuthorityException;
import com.timely.springserver.exception.BadRequestException;
import com.timely.springserver.exception.ResourceNotFoundException;
import com.timely.springserver.model.AuthProvider;
import com.timely.springserver.model.Role;
import com.timely.springserver.model.RoleName;
import com.timely.springserver.model.User;
import com.timely.springserver.model.VerificationToken;
import com.timely.springserver.payload.PasswordRequest;
import com.timely.springserver.payload.ProfileRequest;
import com.timely.springserver.payload.UserRequest;
import com.timely.springserver.repository.RoleRepository;
import com.timely.springserver.repository.UserRepository;
import com.timely.springserver.repository.VerificationTokenRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private VerificationTokenRepository tokenRepository;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
	
	public User getUser(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		
		return user;
	}

	public User getUser(Long creatorId, Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", creatorId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		return user;
	}
	
	public Page<User> getUsers(Long creatorId, Pageable pageable) {
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", creatorId));
		
		List<User> users = userRepository.findAll();
		users = users.stream().filter(user -> user.getId() != creator.getId() && creator.hasAuthority(user)).collect(Collectors.toList());
	
		return new PageImpl<>(users, pageable, users.size());
	}
	
	public User createUser(Long creatorId, UserRequest userRequest, String password) {
		if (userRepository.existsByEmail(userRequest.getEmail())) {
			throw new BadRequestException("Email is already taken!");
		}
		RoleName userRoleName = RoleName.valueOf(userRequest.getRole());
		Role userRole = roleRepository.findByName(userRoleName).orElseThrow(() -> new ResourceNotFoundException("Role", "name", userRequest.getRole()));
		
		User user = new User();
		user.setRole(userRole);
		user.setPassword(passwordEncoder.encode(password));
		
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", creatorId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userRequest.getRole());
		
		return createUser(user, userRequest);
	}
	
	public User createUser(User user, UserRequest userRequest) {
		user.setName(userRequest.getName());
		user.setEmail(userRequest.getEmail());
		user.setPreferredWorkingHourPerDay(userRequest.getPreferredWorkingHourPerDay());
		user.setProvider(AuthProvider.local);
		user.setImage(new byte[0]);

		return userRepository.save(user);
	}
	
	public User updateProfile(Long userId, ProfileRequest profileRequest) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		user.setName(profileRequest.getName());
		user.setEmail(profileRequest.getEmail());
		user.setPreferredWorkingHourPerDay(profileRequest.getPreferredWorkingHourPerDay());
		
		return userRepository.save(user);
	}
	
	public User updateUser(Long creatorId, Long userId, UserRequest userRequest) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", creatorId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		RoleName userRoleName = RoleName.valueOf(userRequest.getRole());
		Role userRole = roleRepository.findByName(userRoleName).orElseThrow(() -> new ResourceNotFoundException("Role", "name", userRequest.getRole()));
		
		return updateUser(user, userRequest, userRole);
	}

	public User updateUser(User user, UserRequest userRequest, Role role) {
		user.setName(userRequest.getName());
		user.setEmail(userRequest.getEmail());
		user.setPreferredWorkingHourPerDay(userRequest.getPreferredWorkingHourPerDay());
		user.setRole(role);
		
		return userRepository.save(user);
	}
	
	public void deleteUser(Long creatorId, Long userId) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", creatorId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		VerificationToken token = tokenRepository.findByUser(user);
		if (token !=null && token.getId() != null) tokenRepository.deleteById(token.getId());
		
		userRepository.deleteById(userId);
	}
	
	public void uploadAvatar(Long creatorId, Long userId, MultipartFile file) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		User creator = userRepository.findById(creatorId).orElseThrow(() ->  new ResourceNotFoundException("Creator", "Id", creatorId));
		
		if (!creator.hasAuthority(user)) throw new AuthorityException(creatorId, userId);
		
		if (!file.isEmpty()) {
	        try {
	            byte[] fileBytes = file.getBytes();
	            user.setImage(fileBytes);
	            userRepository.save(user);
	        }catch (IOException e) {
	            throw new BadRequestException("Error uploading image");
	        }
		}
	}
	
	public User resetUser(final Long userId, String password){
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		user.setPassword(passwordEncoder.encode(password));
		user.setLoginAttemps(0);
		return userRepository.save(user);
	}
	
	public void changePassword(final Long userId, final PasswordRequest passwordRequest) {
		User user = userRepository.findById(userId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		user.setPassword(passwordEncoder.encode(passwordRequest.getPassword()));
		userRepository.save(user);
	}
	
    public void createVerificationTokenForUser(final User user, final String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }
    
    public VerificationToken getVerificationToken(String verificationToken) {
        return tokenRepository.findByToken(verificationToken);
    }
    
    public void saveRegisteredUser(User user) {
        userRepository.save(user);
    }
    
    public void authenticationFailure(String email) {
    	Optional<User> user = userRepository.findByEmail(email);
    	
    	if (user.isPresent()) {
    		User usr = user.get();
    		if (usr.getEmailVerified()) {
	    		usr.setLoginAttemps(usr.getLoginAttemps() + 1);
	    		userRepository.save(usr);
    		}
    	}
    }
    
    public void authenticationSuccess(String email) {
    	Optional<User> user = userRepository.findByEmail(email);
    	
    	if (user.isPresent()) {
    		User usr = user.get();
    		usr.setLoginAttemps(0);
    		userRepository.save(usr);
    	}
    }
}
