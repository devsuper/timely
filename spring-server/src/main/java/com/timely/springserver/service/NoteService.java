package com.timely.springserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.timely.springserver.exception.ResourceNotFoundException;
import com.timely.springserver.model.Note;
import com.timely.springserver.model.Task;
import com.timely.springserver.payload.NoteRequest;
import com.timely.springserver.repository.NoteRepository;
import com.timely.springserver.repository.TaskRepository;

@Service
public class NoteService {
	
	@Autowired
	private NoteRepository noteRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	
	public void createNote(Long userId, Long taskId, NoteRequest request) {
		Task task = taskRepository.findByUserIdAndId(userId, taskId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		
		Note note = new Note();
		note.setDescription(request.getDescription());
		note.setTask(task);
		noteRepository.save(note);
	}
	
	public void deleteNote(Long userId, Long taskId, Long noteId) {
		Task task = taskRepository.findByUserIdAndId(userId, taskId).orElseThrow(() ->  new ResourceNotFoundException("User", "Id", userId));
		Note note = noteRepository.findByTaskIdAndId(task.getId(), noteId).orElseThrow(() ->  new ResourceNotFoundException("Note", "Id", noteId));
		
		noteRepository.deleteById(note.getId());
		
	}
	
	
}
