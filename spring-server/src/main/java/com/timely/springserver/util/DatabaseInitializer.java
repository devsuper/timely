package com.timely.springserver.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.timely.springserver.model.AuthProvider;
import com.timely.springserver.model.Role;
import com.timely.springserver.model.RoleName;
import com.timely.springserver.model.User;
import com.timely.springserver.repository.RoleRepository;
import com.timely.springserver.repository.UserRepository;
import com.timely.springserver.repository.VerificationTokenRepository;

@Component
@ConditionalOnProperty(name = "app.db-init", havingValue = "true")
public class DatabaseInitializer implements CommandLineRunner {
	
	private static final Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);
	@Autowired
	private VerificationTokenRepository verificationTokenRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRepository userRepository;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
	
	@Override
    public void run(String... strings) throws Exception {
		verificationTokenRepository.deleteAll();
		userRepository.deleteAll();
		roleRepository.deleteAll();
		Role managerRole = roleRepository.save(new Role(RoleName.ROLE_MANAGER));
		Role userRole =roleRepository.save(new Role(RoleName.ROLE_USER));
		
		Role adminRole = roleRepository.save(new Role(RoleName.ROLE_ADMIN));
		
		userRepository.deleteAll();

        // Creating user's account
        User user = new User();
        user.setName("Admin");
        user.setEmail("admin@admin.pt");
        user.setEmailVerified(true);
        user.setProvider(AuthProvider.local);
        user.setRole(adminRole);
        user.setPassword(passwordEncoder.encode("password"));
        user.setImage(new byte[0]);
        userRepository.save(user);
        
        User rUser = new User();
        rUser.setName("user");
        rUser.setEmail("user@user.pt");
        rUser.setEmailVerified(true);
        rUser.setProvider(AuthProvider.local);
        rUser.setRole(userRole);
        rUser.setPassword(passwordEncoder.encode("password"));
        rUser.setImage(new byte[0]);
        userRepository.save(rUser);
        
        User mUser = new User();
        mUser.setName("manager");
        mUser.setEmail("manager@manager.pt");
        mUser.setEmailVerified(true);
        mUser.setProvider(AuthProvider.local);
        mUser.setRole(managerRole);
        mUser.setPassword(passwordEncoder.encode("password"));
        mUser.setImage(new byte[0]);
        userRepository.save(mUser);
		
		logger.info(" -- Database has been initialized");
	}
}
