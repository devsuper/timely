package com.timely.springserver.security.oauth2;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.timely.springserver.exception.BadRequestException;
import com.timely.springserver.exception.OAuth2AuthenticationProcessingException;
import com.timely.springserver.model.AuthProvider;
import com.timely.springserver.model.Role;
import com.timely.springserver.model.RoleName;
import com.timely.springserver.model.User;
import com.timely.springserver.repository.RoleRepository;
import com.timely.springserver.repository.UserRepository;
import com.timely.springserver.security.UserPrincipal;
import com.timely.springserver.security.oauth2.user.OAuth2UserInfo;
import com.timely.springserver.security.oauth2.user.OAuth2UserInfoFactory;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

        try {
            return processOAuth2User(oAuth2UserRequest, oAuth2User);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (Exception ex) {
            // Throwing an instance of AuthenticationException will trigger the OAuth2AuthenticationFailureHandler
            throw new InternalAuthenticationServiceException(ex.getMessage(), ex.getCause());
        }
    }

    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) throws Exception {
        OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(oAuth2UserRequest.getClientRegistration().getRegistrationId(), oAuth2User.getAttributes());
        if(StringUtils.isEmpty(oAuth2UserInfo.getEmail())) {
            throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
        }

        Optional<User> userOptional = userRepository.findByEmail(oAuth2UserInfo.getEmail());
        User user;
        if(userOptional.isPresent()) {
            user = userOptional.get();
            if(!user.getProvider().equals(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()))) {
                throw new OAuth2AuthenticationProcessingException("Looks like you're signed up with " +
                        user.getProvider() + " account. Please use your " + user.getProvider() +
                        " account to login.");
            }
            user = updateExistingUser(user, oAuth2UserInfo);
        } else {
            user = registerNewUser(oAuth2UserRequest, oAuth2UserInfo);
        }

        return UserPrincipal.create(user, oAuth2User.getAttributes());
    }

    private User registerNewUser(OAuth2UserRequest oAuth2UserRequest, OAuth2UserInfo oAuth2UserInfo) throws Exception {    	
    	Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
				.orElseThrow(() -> new BadRequestException("User Role not set."));
    	
    	User user = new User();

        user.setProvider(AuthProvider.valueOf(oAuth2UserRequest.getClientRegistration().getRegistrationId()));
        user.setProviderId(oAuth2UserInfo.getId());
        user.setName(oAuth2UserInfo.getName());
        user.setEmail(oAuth2UserInfo.getEmail());
        user.setRole(userRole);
        user.setImage(imageConvert(oAuth2UserInfo.getImageUrl()));
        return userRepository.save(user);
    }

    private User updateExistingUser(User existingUser, OAuth2UserInfo oAuth2UserInfo) throws Exception {

        existingUser.setName(oAuth2UserInfo.getName());
        existingUser.setImage(imageConvert(oAuth2UserInfo.getImageUrl()));
        return userRepository.save(existingUser);
    }
    
    public byte[] imageConvert(String urlText) {
    	try {
	        URL url = new URL(urlText);
	        ByteArrayOutputStream output = new ByteArrayOutputStream();
	         
	        try (InputStream inputStream = url.openStream()) {
	            int n = 0;
	            byte [] buffer = new byte[ 1024 ];
	            while (-1 != (n = inputStream.read(buffer))) {
	                output.write(buffer, 0, n);
	            }
	        }
	        return output.toByteArray();
    	} catch(Exception e ) {
    		System.err.println(e.getMessage());
    	}
     
        return new byte[0];
    }
}

