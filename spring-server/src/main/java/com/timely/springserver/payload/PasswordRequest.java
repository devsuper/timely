package com.timely.springserver.payload;

import javax.validation.constraints.NotBlank;

public class PasswordRequest  {
	
    @NotBlank
    private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
