package com.timely.springserver.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class ProfileRequest  {
	
    @NotBlank
    private String name;

    @NotBlank
    @Email
    private String email;
	
	private Long preferredWorkingHourPerDay;

	public Long getPreferredWorkingHourPerDay() {
		return preferredWorkingHourPerDay;
	}

	public void setPreferredWorkingHourPerDay(Long preferredWorkingHourPerDay) {
		this.preferredWorkingHourPerDay = preferredWorkingHourPerDay;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
