package com.timely.springserver.payload;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.timely.springserver.model.Note;

public class TaskResponse {
	
    private Long id;
    
	private String description;
	
	private Instant startDate;
	
	private Instant endDate;
	
	private Long duration;
	
	private Boolean exceedDailyHour;
		
	private List<Note> notes = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Instant getStartDate() {
		return startDate;
	}

	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}

	public void setEndDate(Instant endDate) {
		this.endDate = endDate;
	}

	public List<Note> getNotes() {
		return notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Boolean getExceedDailyHour() {
		return exceedDailyHour;
	}

	public void setExceedDailyHour(Boolean exceedDailyHour) {
		this.exceedDailyHour = exceedDailyHour;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}
}
