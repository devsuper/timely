package com.timely.springserver.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class UserRequest  {
	
    @NotBlank
    private String name;

    @NotBlank
    @Email
    private String email;
	
	@NotBlank
	private String role;
	
	private Boolean generatePassword;

	private Long preferredWorkingHourPerDay;

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getGeneratePassword() {
		return generatePassword;
	}

	public void setGeneratePassword(Boolean generatePassword) {
		this.generatePassword = generatePassword;
	}

	public Long getPreferredWorkingHourPerDay() {
		return preferredWorkingHourPerDay;
	}

	public void setPreferredWorkingHourPerDay(Long preferredWorkingHourPerDay) {
		this.preferredWorkingHourPerDay = preferredWorkingHourPerDay;
	}
}
