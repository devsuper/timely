package com.timely.springserver.payload;

import java.time.Instant;

import javax.validation.constraints.NotNull;

public class TaskRequest  {

	@NotNull
	private String description;
	
	@NotNull
	private Instant startDate;
	
	private Instant endDate;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Instant getStartDate() {
		return startDate;
	}

	public void setStartDate(Instant startDate) {
		this.startDate = startDate;
	}

	public Instant getEndDate() {
		return endDate;
	}

	public void setEndDate(Instant endDate) {
		this.endDate = endDate;
	}

}
