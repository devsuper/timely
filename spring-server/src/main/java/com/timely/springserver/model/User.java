package com.timely.springserver.model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email")
})
public class User implements Serializable {
	
	private static final long serialVersionUID = -1900500912838126804L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Email
    @Column(nullable = false)
    private String email;

    @Lob
    @Column(columnDefinition="mediumblob")
    private byte[] image;

    @Column(nullable = false)
    private Boolean emailVerified = false;

    @JsonIgnore
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider provider;

    private String providerId;
    
	@ManyToOne
	private Role role;
	
	private Integer loginAttemps = 0;
	
	private Long preferredWorkingHourPerDay;
	
    @OneToMany(cascade = CascadeType.ALL, 
            mappedBy = "user", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Task> tasks = new ArrayList<>();
    
    public boolean hasAuthority(User user) {
    	return (this.id.equals(user.getId()) ||
    			this.role.getName().equals(RoleName.ROLE_ADMIN) ||
    			(this.role.getName().equals(RoleName.ROLE_MANAGER) && user.getRole().getName().equals(RoleName.ROLE_USER))); 
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AuthProvider getProvider() {
        return provider;
    }

    public void setProvider(AuthProvider provider) {
        this.provider = provider;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}


	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public Integer getLoginAttemps() {
		return loginAttemps;
	}

	public void setLoginAttemps(Integer loginAttemps) {
		this.loginAttemps = loginAttemps;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Long getPreferredWorkingHourPerDay() {
		return preferredWorkingHourPerDay;
	}

	public void setPreferredWorkingHourPerDay(Long preferredWorkingHourPerDay) {
		this.preferredWorkingHourPerDay = preferredWorkingHourPerDay;
	}
}
