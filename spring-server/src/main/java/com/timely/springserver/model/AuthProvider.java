package com.timely.springserver.model;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}
