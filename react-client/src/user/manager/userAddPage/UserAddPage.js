import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import UserForm from "../common/UserForm";
import { startAddUser } from "../../../actions/user";

export class UserAddPage extends React.Component {
  onSubmit = (user, avatar) => {
    this.props
      .addUser(user, avatar)
      .then(message => {
        Alert.success(message);
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <h1 className="card-header text-center">New User</h1>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 offset-md-2">
                    <UserForm onSubmit={this.onSubmit} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addUser: (user, avatar) => dispatch(startAddUser(user, avatar))
});

export default connect(
  undefined,
  mapDispatchToProps
)(UserAddPage);
