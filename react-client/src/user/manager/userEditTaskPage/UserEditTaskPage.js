import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import TaskForm from "../../common/TaskForm";
import {
  startGetTask,
  startEditUserTask,
  startRemoveUserTask
} from "../../../actions/usertasks";

export class UserEditTaskPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: true,
      task: undefined
    };
  }
  componentDidMount() {
    this.props
      .getTask(this.props.userId, this.props.taskId)
      .then(task => {
        this.setState({ task: task, busy: false });
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  }
  onSubmit = task => {
    this.props
      .editTask(this.props.userId, this.props.taskId, task)
      .then(message => {
        Alert.success(message);
        this.props.history.push(`/users/${this.props.userId}/tasks`);
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  onRemove = () => {
    this.props
      .removeTask(this.props.userId, this.props.taskId)
      .then(message => {
        Alert.success(message);
        this.props.history.push(`/users/${this.props.userId}/tasks`);
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    if (this.state.busy) {
      return <div>BUSY</div>;
    }
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <h1 className="card-header text-center">Edit User Task</h1>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 offset-md-2">
                    <TaskForm
                      task={this.state.task}
                      onSubmit={this.onSubmit}
                      onRemove={this.onRemove}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => ({
  userId: props.match.params.id,
  taskId: props.match.params.taskId
});

const mapDispatchToProps = (dispatch, props) => ({
  getTask: (userId, id) => dispatch(startGetTask(userId, id)),
  editTask: (userId, id, task) => dispatch(startEditUserTask(userId, id, task)),
  removeTask: (userId, id) => dispatch(startRemoveUserTask(userId, id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEditTaskPage);
