import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import TaskForm from "../../common/TaskForm";
import { startAddUserTask } from "../../../actions/usertasks";

export class UserAddTaskPage extends React.Component {
  onSubmit = task => {
    this.props
      .addTask(this.props.userId, task)
      .then(message => {
        Alert.success(message);
        this.props.history.push(`/users/${this.props.userId}/tasks`);
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <h1 className="card-header text-center">Add User Task</h1>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 offset-md-2">
                    <TaskForm onSubmit={this.onSubmit} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    userId: props.match.params.id
  };
};

const mapDispatchToProps = dispatch => ({
  addTask: (userId, task) => dispatch(startAddUserTask(userId, task))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAddTaskPage);
