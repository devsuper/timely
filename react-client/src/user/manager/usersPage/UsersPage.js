import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";
import Alert from "react-s-alert";
import { startGetUsers, resetPassword } from "../../../actions/user";
import LoadingIndicator from "../../../common/LoadingIndicator";
import UserList from "../common/UserList";

export class UsersPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0
    };
  }

  renderList = () => {
    const filter = {
      page: this.state.page
    };
    this.props.getUsers(filter).then(
      result => {
        console.log("Success", result);
      },
      error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      }
    );
  };

  handlePasswordReset = userId => {
    this.props.resetPassword(userId).then(
      message => {
        Alert.success(message);
      },
      error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      }
    );
  };

  handlePageClick = data => {
    let page = data.selected;
    this.setState({ page: page }, () => {
      this.renderList();
    });
  };

  componentDidMount() {
    this.renderList();
  }

  render() {
    return (
      <div className="col">
        {this.props.busy && <LoadingIndicator />}
        {!this.props.busy && (
          <React.Fragment>
            <div className="jumbotron text-center">
              <h1 className="display-4 mb-2 mt-2 ">Users</h1>
              <hr className="my-4" />
              <div className="row">
                <div className="col-md-4 offset-md-4 text-center">
                  <Link
                    to="/users/new"
                    className="btn btn-primary btn-block btn-lg"
                  >
                    <i className="fas fa-plus" /> New User
                  </Link>
                </div>
              </div>
            </div>
            <UserList
              users={this.props.users}
              passwordReset={this.handlePasswordReset}
            />
            {this.props.totalPages > 1 && (
              <nav aria-label="Page navigation example">
                <ReactPaginate
                  previousLabel={"Previous"}
                  nextLabel={"Next"}
                  breakLabel={"..."}
                  forcePage={this.state.page}
                  pageCount={this.props.totalPages}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={this.handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  subContainerClassName={"page-item"}
                  activeClassName={"page-item active"}
                  nextClassName={"page-item"}
                  previousClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  nextLinkClassName={"page-link"}
                  previousLinkClassName={"page-link"}
                />
              </nav>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    busy: state.users.busy,
    users: state.users.list,
    totalPages: state.users.totalPages
  };
};

const mapDispatchToProps = dispatch => ({
  getUsers: filter => dispatch(startGetUsers(filter)),
  resetPassword: userId => dispatch(resetPassword(userId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersPage);
