import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import { startInvite } from "../../../actions/user";

export class InvitePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ""
    };
  }
  handleInputChange = event => {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;
    this.setState({
      [inputName]: inputValue
    });
  };
  onSubmit = e => {
    e.preventDefault();
    this.props
      .invite(this.state.email)
      .then(message => {
        Alert.success(message);
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <h1 className="card-header text-center">Invite User</h1>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 offset-md-2">
                    <form onSubmit={this.onSubmit}>
                      <div className="form-group row">
                        <label className="col-sm-2 col-form-label">Email</label>
                        <div className="col-sm-10">
                          <input
                            id="email"
                            name="email"
                            minLength={4}
                            maxLength={40}
                            required={true}
                            className="form-control"
                            placeholder="Enter email..."
                            type={"email"}
                            value={this.state.email}
                            onChange={this.handleInputChange}
                          />
                        </div>
                      </div>
                      <hr className="my-4" />
                      <div className="col-sm-6 offset-sm-3">
                        <button
                          type="submit"
                          className="btn btn-primary btn-block mb-2"
                        >
                          Invite
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  invite: email => dispatch(startInvite(email))
});

export default connect(
  undefined,
  mapDispatchToProps
)(InvitePage);
