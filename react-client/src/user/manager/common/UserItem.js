import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { hasRole } from "../../../selectors/auth";

export class UserItem extends React.Component {
  onPasswordReset = e => {
    e.preventDefault();
    this.props.passwordReset(this.props.user.id);
  };
  render() {
    return (
      <div id={this.props.user.id} className="card mb-3">
        <div className="card-body">
          <div className="row">
            <div className="col-md-2 col-sm-2 text-center">
              {this.props.user.image ? (
                <img
                  className="rounded-circle"
                  src={"data:image/jpg;base64," + this.props.user.image}
                  alt={this.props.user.name}
                  width="150"
                  height="150"
                />
              ) : (
                <img
                  className="rounded-circle"
                  src="https://via.placeholder.com/150/"
                  width="150"
                  height="150"
                  alt={this.props.user.name}
                />
              )}
            </div>
            <div className="col-md-7 col-sm-7">
              <h3 className="d-inline-block">{this.props.user.name}</h3>{" "}
              {this.props.user.role && (
                <span className="badge bg-primary text-white align-top">
                  {this.props.user.role.name}
                </span>
              )}
              <p>
                {!this.props.user.emailVerified && (
                  <span className="badge bg-warning">Email not verified</span>
                )}
                {this.props.user.loginAttemps >= 3 && (
                  <span className="badge bg-warning">
                    Exceed login attemps. Tries: {this.props.user.loginAttemps}
                  </span>
                )}
              </p>
              <dl class="row">
                <dt class="col-sm-3">Email:</dt>
                <dd class="col-sm-9">{this.props.user.email}</dd>
                <dt class="col-sm-3">Hours per day:</dt>
                <dd class="col-sm-9">
                  {(
                    "0" +
                    Math.floor(this.props.user.preferredWorkingHourPerDay / 60)
                  ).slice(-2) +
                    ":" +
                    (
                      "0" +
                      (this.props.user.preferredWorkingHourPerDay % 60)
                    ).slice(-2)}
                </dd>
              </dl>
            </div>
            <div className="col-md-2 offset-md-1">
              <p>
                <Link
                  to={`users/edit/${this.props.user.id}`}
                  className="btn btn-primary btn-block"
                >
                  <i className="fas fa-edit" />
                  Edit
                </Link>
              </p>
              {this.props.isAdmin && (
                <p>
                  <Link
                    to={`users/${this.props.user.id}/tasks`}
                    className="btn btn-primary btn-block btn-md"
                  >
                    View Tasks
                  </Link>
                </p>
              )}
              {this.props.user.loginAttemps >= 3 && (
                <p>
                  <button
                    type="button"
                    className="btn btn-primary btn-block btn-md "
                    onClick={this.onPasswordReset}
                  >
                    Reset password
                  </button>
                </p>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAdmin: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_ADMIN"])
});

export default connect(mapStateToProps)(UserItem);
