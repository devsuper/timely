import React from "react";
import { connect } from "react-redux";
import { hasRole } from "../../../selectors/auth";

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.user ? props.user.name : "",
      email: props.user ? props.user.email : "",
      role: props.user && props.user.role ? props.user.role.name : "ROLE_USER",
      hours: props.user
        ? Math.floor(props.user.preferredWorkingHourPerDay / 60)
        : 1,
      minutes: props.user ? props.user.preferredWorkingHourPerDay % 60 : 0,
      initialImage: props.user ? props.user.image : "",
      generatePassword: false,
      file: "",
      image: "",
      error: ""
    };
  }
  createSelectItems = (start, max) => {
    let items = [];
    for (let i = start; i <= max; i++) {
      items.push(
        <option key={i} value={i}>
          {i}
        </option>
      );
    }
    return items;
  };
  handleInputChange = event => {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;
    this.setState({
      [inputName]: inputValue
    });
  };
  handleAvatarChange = e => {
    e.preventDefault();
    if (e.target.files && e.target.files[0]) {
      let reader = new FileReader();
      let file = e.target.files[0];
      reader.onloadend = () => {
        this.setState({
          file: file,
          image: reader.result
        });
      };
      reader.readAsDataURL(file);
    }
  };
  onSubmit = e => {
    e.preventDefault();
    if (this.state.file && this.state.file.size >= 2000000) {
      this.setState({ error: "File size exceeds limit of 2MB." });
      return;
    }
    const minutes =
      parseInt(this.state.hours, 10) * 60 + parseInt(this.state.minutes, 10);
    this.props.onSubmit(
      {
        name: this.state.name,
        email: this.state.email,
        role: this.state.role,
        preferredWorkingHourPerDay: minutes,
        generatePassword: this.state.generatePassword
      },
      this.state.file
    );
  };
  onRemove = e => {
    e.preventDefault();
    this.props.onRemove();
  };
  render() {
    return (
      <form onSubmit={this.onSubmit}>
        {this.state.error}
        <div className="row mb-2">
          <div className="col-md-12 text-center">
            {this.state.image ? (
              <img
                className="rounded-circle"
                src={this.state.image}
                width="150"
                height="150"
                alt={this.state.name}
              />
            ) : (
              <img
                className="rounded-circle"
                src={
                  this.state.initialImage
                    ? "data:image/jpg;base64," + this.state.initialImage
                    : "https://via.placeholder.com/150"
                }
                width="150"
                height="150"
                alt={this.state.name}
              />
            )}
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">Avatar</label>
          <div className="col-sm-9">
            <div className="custom-file">
              <input
                type="file"
                className="custom-file-input"
                name="file"
                accept="image/*"
                onChange={this.handleAvatarChange}
              />
              <label className="custom-file-label">Choose file</label>
            </div>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">Name</label>
          <div className="col-sm-9">
            <input
              id="name"
              name="name"
              minLength={4}
              maxLength={40}
              required={true}
              className="form-control"
              placeholder="Enter your name..."
              type={"text"}
              value={this.state.name}
              onChange={this.handleInputChange}
            />
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">Email</label>
          <div className="col-sm-9">
            <input
              id="email"
              name="email"
              minLength={4}
              maxLength={40}
              required={true}
              className="form-control"
              placeholder="Enter your email..."
              type={"email"}
              value={this.state.email}
              onChange={this.handleInputChange}
            />
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">
            Hours per day (HH:mm)
          </label>
          <div className="col-sm-3">
            <div className="input-group ">
              <select
                name="hours"
                value={this.state.hours}
                className="custom-select"
                onChange={this.handleInputChange}
              >
                {this.createSelectItems(1, 24)}
              </select>
              <div className="input-group-append">
                <div className="input-group-text">Hours</div>
              </div>
            </div>
          </div>
          <div className="col-sm-3">
            <div className="input-group ">
              <select
                name="minutes"
                value={this.state.minutes}
                className="custom-select col"
                onChange={this.handleInputChange}
              >
                {this.createSelectItems(0, 60)}
              </select>
              <div className="input-group-append">
                <div className="input-group-text">Minutes</div>
              </div>
            </div>
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-3 col-form-label">Role</label>
          <div className="col-sm-9">
            <select
              name="role"
              value={this.state.role}
              className="custom-select"
              onChange={this.handleInputChange}
            >
              {this.props.isManager && (
                <React.Fragment>
                  <option>ROLE_USER</option>
                  <option>ROLE_MANAGER</option>
                </React.Fragment>
              )}
              {this.props.isAdmin && (
                <React.Fragment>
                  <option>ROLE_USER</option>
                  <option>ROLE_MANAGER</option>
                  <option>ROLE_ADMIN</option>
                </React.Fragment>
              )}
            </select>
          </div>
        </div>
        <hr className="my-4" />
        <div className="col-sm-6 offset-sm-3">
          <button type="submit" className="btn btn-primary btn-block mb-2">
            Submit
          </button>
        </div>
        {this.props.user && (
          <React.Fragment>
            <div className="col-sm-6 offset-sm-3">
              <button
                type="button"
                className="btn btn-danger btn-block mb-2"
                onClick={this.onRemove}
                intent="Danger"
              >
                Remove
              </button>
            </div>
          </React.Fragment>
        )}
      </form>
    );
  }
}

const mapStateToProps = state => ({
  isAdmin: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_ADMIN"]),
  isManager: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_MANAGER"])
});

export default connect(mapStateToProps)(UserForm);
