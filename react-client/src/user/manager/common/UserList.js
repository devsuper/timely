import React from "react";
import UserItem from "./UserItem";

export const UserList = props => (
  <div className="container-fluid">
    <div className="row">
      <div className="col-12">
        {props.users.length === 0 ? (
          <p>No users</p>
        ) : (
          props.users.map(user => {
            return (
              <UserItem
                key={user.id}
                user={user}
                passwordReset={props.passwordReset}
              />
            );
          })
        )}
      </div>
    </div>
  </div>
);
export default UserList;
