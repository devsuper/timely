import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import UserForm from "../common/UserForm";
import {
  startGetUser,
  startEditUser,
  startRemoveUser
} from "../../../actions/user";

export class UserEditPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: props.id,
      busy: true,
      user: undefined
    };
  }
  componentDidMount() {
    this.props
      .getUser(this.state.userId)
      .then(user => {
        this.setState({ user: user, busy: false });
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  }
  onSubmit = (user, avatar) => {
    this.props
      .editUser(this.state.userId, user, avatar)
      .then(message => {
        Alert.success(message);
        this.props.history.push("/users");
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  onRemove = () => {
    this.props
      .removeUser(this.state.userId)
      .then(message => {
        Alert.success(message);
        this.props.history.push("/users");
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    if (this.state.busy) {
      return <div>BUSY</div>;
    }
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <h1 className="card-header text-center">Edit User</h1>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 offset-md-2">
                    <UserForm
                      user={this.state.user}
                      onSubmit={this.onSubmit}
                      onRemove={this.onRemove}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => ({
  id: props.match.params.id
});

const mapDispatchToProps = (dispatch, props) => ({
  getUser: id => dispatch(startGetUser(id)),
  editUser: (id, user, avatar) => dispatch(startEditUser(id, user, avatar)),
  removeUser: id => dispatch(startRemoveUser(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEditPage);
