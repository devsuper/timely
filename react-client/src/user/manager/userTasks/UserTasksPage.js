import React from "react";
import ReactPaginate from "react-paginate";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Alert from "react-s-alert";
import {
  startGetUserTasks,
  startEditUserTask
} from "../../../actions/usertasks";
import LoadingIndicator from "../../../common/LoadingIndicator";
import TaskList from "../../../common/TaskList";
import TaskFilter from "../../task/tasksPage/TaskFilter";

export class UserTasksPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: false,
      page: 0
    };
  }

  renderList = () => {
    const filter = {
      page: this.state.page,
      startDate: this.props.startDate,
      endDate: this.props.endDate
    };
    this.props.getUserTasks(this.props.userId, filter).then(
      result => {
        console.log("Success", result);
      },
      error => {
        console.log("Error getting tasks", error);
      }
    );
  };

  handleOnFinish = (id, request) => {
    this.props
      .editUserTask(this.props.userId, id, request)
      .then(message => {
        Alert.success(message);
        this.renderList();
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };

  handlePageClick = data => {
    let page = data.selected;
    this.setState({ page: page }, () => {
      this.renderList();
    });
  };

  componentDidMount() {
    this.renderList();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.endDate !== this.props.endDate ||
      prevProps.startDate !== this.props.startDate
    ) {
      this.renderList();
    }
  }

  render() {
    return (
      <div className="col">
        {this.props.busy && <LoadingIndicator />}
        {!this.props.busy && (
          <React.Fragment>
            <div className="jumbotron text-center">
              <h1 className="display-4 mb-2 mt-2 ">User Tasks</h1>
              <hr className="my-4" />
              <div className="row">
                <div className="col-md-4 offset-md-4 text-center">
                  <Link
                    to={`/users/${this.props.userId}/tasks/add`}
                    className="btn btn-primary btn-block btn-lg"
                  >
                    <i class="fas fa-plus" /> Add Task
                  </Link>
                </div>
              </div>
              <div className="row mt-4">
                <TaskFilter />
              </div>
            </div>
            <TaskList tasks={this.props.tasks} onFinish={this.handleOnFinish} />
            {this.props.totalPages > 1 && (
              <nav aria-label="Page navigation example">
                <ReactPaginate
                  previousLabel={"Previous"}
                  nextLabel={"Next"}
                  breakLabel={"..."}
                  forcePage={this.state.page}
                  pageCount={this.props.totalPages}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={this.handlePageClick}
                  containerClassName={"pagination justify-content-center"}
                  subContainerClassName={"page-item"}
                  activeClassName={"page-item active"}
                  nextClassName={"page-item"}
                  previousClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  nextLinkClassName={"page-link"}
                  previousLinkClassName={"page-link"}
                />
              </nav>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    busy: state.userstasks.busy,
    tasks: state.userstasks.list,
    totalPages: state.userstasks.totalPages,
    startDate: state.taskfilters.startDate,
    endDate: state.taskfilters.endDate,
    userId: props.match.params.id
  };
};

const mapDispatchToProps = dispatch => ({
  getUserTasks: (userId, filter) => dispatch(startGetUserTasks(userId, filter)),
  editUserTask: (userId, taskId, request) =>
    dispatch(startEditUserTask(userId, taskId, request))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserTasksPage);
