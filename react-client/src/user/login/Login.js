import React, { Component } from "react";
import { connect } from "react-redux";
import "./Login.css";
import { GOOGLE_AUTH_URL, GITHUB_AUTH_URL } from "../../constants";
import { startLogin } from "../../actions/auth";
import { Link, Redirect } from "react-router-dom";
import LoadingIndicator from "../../common/LoadingIndicator";
import googleLogo from "../../img/google-logo.png";
import githubLogo from "../../img/github-logo.png";
import Alert from "react-s-alert";

export class Login extends Component {
  componentDidMount() {
    if (this.props.location.state && this.props.location.state.error) {
      setTimeout(() => {
        Alert.error(this.props.location.state.error, {
          timeout: 5000
        });
        this.props.history.replace({
          pathname: this.props.location.pathname,
          state: {}
        });
      }, 100);
    }
  }

  render() {
    if (this.props.authenticated) {
      return (
        <Redirect
          to={{
            pathname: "/",
            state: { from: this.props.location }
          }}
        />
      );
    }

    return (
      <div className="row justify-content-center text-center mt-2">
        <div className="col-5">
          <div className="card">
            <div className="card-body">
              <h1 className="login-title">Login to timely</h1>
              <SocialLogin />
              <div className="or-separator">
                <span className="or-text">OR</span>
              </div>
              <LoginForm {...this.props} />
              <span className="signup-link">
                New user? <Link to="/signup">Sign up!</Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class SocialLogin extends Component {
  render() {
    return (
      <div className="social-login">
        <a className="btn btn-block social-btn google" href={GOOGLE_AUTH_URL}>
          <img src={googleLogo} alt="Google" /> Log in with Google
        </a>
        <a className="btn btn-block social-btn github" href={GITHUB_AUTH_URL}>
          <img src={githubLogo} alt="Github" /> Log in with Github
        </a>
      </div>
    );
  }
}

export class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;

    this.setState({
      [inputName]: inputValue
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      busy: true
    });

    const loginRequest = Object.assign({}, this.state);

    this.props
      .startLogin(loginRequest)
      .then(response => {
        this.setState({
          busy: false
        });
        this.props.history.push("/");
      })
      .catch(error => {
        this.setState({
          busy: false
        });
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  }

  render() {
    if (this.state.busy) {
      return <LoadingIndicator />;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-item">
          <input
            type="email"
            name="email"
            className="form-control"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleInputChange}
            required
          />
        </div>
        <div className="form-item">
          <input
            type="password"
            name="password"
            className="form-control"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleInputChange}
            required
          />
        </div>
        <div className="form-item">
          <button type="submit" className="btn btn-block btn-primary">
            Login
          </button>
        </div>
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  startLogin: loginRequest => dispatch(startLogin(loginRequest))
});

export default connect(
  undefined,
  mapDispatchToProps
)(Login);
