import React from "react";
import moment from "moment";
import Alert from "react-s-alert";
import { DatePicker, TimePrecision } from "@blueprintjs/datetime";

export default class TaskForm extends React.Component {
  constructor(props) {
    super(props);
    let startDate =
      props.task && props.task.startDate != null
        ? moment(props.task.startDate)
        : moment();
    this.state = {
      description: props.task ? props.task.description : "",
      ongoing: props.task && props.task.endDate == null ? true : false,
      startDate: startDate.toDate(),
      endDate:
        props.task && props.task.endDate != null
          ? moment(props.task.endDate).toDate()
          : startDate.add(1, "minutes").toDate()
    };
  }
  handleInputChange = event => {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;
    this.setState({
      [inputName]: inputValue
    });
  };
  handleOnGoingChange = event => {
    this.setState({
      ongoing: event.target.checked
    });
  };
  handleStartDateChange = date => {
    this.setState({
      startDate: date
    });
  };
  handleEndDateChange = date => {
    this.setState({
      endDate: date
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    let request = {
      description: this.state.description,
      startDate: this.state.startDate
    };
    if (!this.state.ongoing) {
      if (this.state.endDate.getTime() < this.state.startDate.getTime()) {
        Alert.error("The end date must be after the start date");
        return;
      }
      request.endDate = this.state.endDate;
    }
    this.props.onSubmit(request);
  };
  onRemove = e => {
    e.preventDefault();
    this.props.onRemove();
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group row">
          <label className="col-sm-2 col-form-label">Description</label>
          <div className="col-sm-10">
            <input
              id="description"
              name="description"
              minLength={4}
              maxLength={40}
              required={true}
              className="form-control"
              placeholder="Enter your description..."
              type={"text"}
              value={this.state.description}
              onChange={this.handleInputChange}
            />
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-2 col-form-label">Start Date</label>
          <div className="col-sm-4">
            <DatePicker
              id="startDate"
              onChange={this.handleStartDateChange}
              value={this.state.startDate}
              timePrecision={TimePrecision.MINUTE}
              maxDate={this.state.endDate}
            />
          </div>
        </div>
        <div className="form-group row">
          <label className="col-sm-2 col-form-label">OnGoing</label>
          <div className="col-sm-10">
            <input
              type="checkbox"
              name="ongoing"
              checked={this.state.ongoing}
              className="form-check-input"
              onChange={this.handleOnGoingChange}
            />
          </div>
        </div>
        {!this.state.ongoing && (
          <div className="form-group row">
            <label className="col-sm-2 col-form-label">End Date</label>
            <div className="col-sm-4">
              <DatePicker
                id="endDate"
                onChange={this.handleEndDateChange}
                value={this.state.endDate}
                timePrecision={TimePrecision.MINUTE}
                minDate={this.state.startDate}
              />
            </div>
          </div>
        )}
        <hr className="my-4" />
        <div className="col-sm-6 offset-sm-3">
          <button type="submit" className="btn btn-primary btn-block mb-2">
            Submit
          </button>
        </div>
        {this.props.task && (
          <React.Fragment>
            <hr className="my-4" />
            <div className="col-sm-6 offset-sm-3">
              <button
                type="button"
                className="btn btn-danger btn-block mb-2"
                onClick={this.onRemove}
                intent="Danger"
              >
                Remove
              </button>
            </div>
          </React.Fragment>
        )}
      </form>
    );
  }
}
