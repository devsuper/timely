import React, { Component } from "react";
import "./Signup.css";
import { Link, Redirect } from "react-router-dom";
import { GOOGLE_AUTH_URL, GITHUB_AUTH_URL } from "../../constants";
import { startSignUpRequst } from "../../actions/auth";
import googleLogo from "../../img/google-logo.png";
import githubLogo from "../../img/github-logo.png";
import LoadingIndicator from "../../common/LoadingIndicator";
import Alert from "react-s-alert";

class Signup extends Component {
  render() {
    if (this.props.authenticated) {
      return (
        <Redirect
          to={{
            pathname: "/",
            state: { from: this.props.location }
          }}
        />
      );
    }

    return (
      <div className="row justify-content-center text-center mt-2">
        <div className="col-4">
          <div className="card">
            <div className="card-body">
              <h1 className="signup-title">Signup</h1>
              <SocialSignup />
              <div className="or-separator">
                <span className="or-text">OR</span>
              </div>
              <SignupForm {...this.props} />
              <span className="login-link">
                Already have an account? <Link to="/login">Login!</Link>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

class SocialSignup extends Component {
  render() {
    return (
      <div className="social-signup">
        <a className="btn btn-block social-btn google" href={GOOGLE_AUTH_URL}>
          <img src={googleLogo} alt="Google" /> Sign up with Google
        </a>
        <a className="btn btn-block social-btn github" href={GITHUB_AUTH_URL}>
          <img src={githubLogo} alt="Github" /> Sign up with Github
        </a>
      </div>
    );
  }
}

class SignupForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      busy: false
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const inputName = target.name;
    const inputValue = target.value;

    this.setState({
      [inputName]: inputValue
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({
      busy: true
    });
    const signUpRequest = Object.assign({}, this.state);

    startSignUpRequst(signUpRequest)
      .then(response => {
        this.setState({
          busy: false
        });
        Alert.success(
          "You're successfully registered. Please login to continue!"
        );
        this.props.history.push("/login");
      })
      .catch(error => {
        this.setState({
          busy: false
        });
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  }

  render() {
    if (this.state.busy) {
      return <LoadingIndicator />;
    }
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-item">
          <input
            type="text"
            name="name"
            className="form-control"
            maxLength="20"
            placeholder="Name"
            value={this.state.name}
            onChange={this.handleInputChange}
            required
          />
        </div>
        <div className="form-item">
          <input
            type="email"
            name="email"
            maxLength="60"
            className="form-control"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleInputChange}
            required
          />
        </div>
        <div className="form-item">
          <input
            type="password"
            name="password"
            maxLength="30"
            className="form-control"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleInputChange}
            required
          />
        </div>
        <div className="form-item">
          <button type="submit" className="btn btn-block btn-primary">
            Sign Up
          </button>
        </div>
      </form>
    );
  }
}

export default Signup;
