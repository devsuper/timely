import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import ProfileForm from "./ProfileForm";
import { getProfile, startEditProfile } from "../../actions/auth";

export class EditProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: true,
      user: undefined
    };
  }
  componentDidMount() {
    this.props
      .getProfile()
      .then(user => {
        this.setState({ user: user, busy: false });
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  }
  onSubmit = (user, avatar) => {
    this.props
      .editUser(user, avatar)
      .then(message => {
        Alert.success(message);
        this.props.history.push("/profile");
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    if (this.state.busy) {
      return <div>BUSY</div>;
    }
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <h1 className="card-header text-center">Edit Profile</h1>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 offset-md-2">
                    <ProfileForm
                      user={this.state.user}
                      onSubmit={this.onSubmit}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  getProfile: () => dispatch(getProfile()),
  editUser: (user, avatar) => dispatch(startEditProfile(user, avatar))
});

export default connect(
  undefined,
  mapDispatchToProps
)(EditProfilePage);
