import React from "react";
import * as moment from "moment";
import "moment-duration-format";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Alert from "react-s-alert";
import LoadingIndicator from "../../common/LoadingIndicator";
import { getProfile } from "../../actions/auth";

export class ProfilePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: true,
      user: undefined
    };
  }
  createHoursPerDay = () => {
    console.log(this.state.user.preferredWorkingHourPerDay);
    if (!this.state.user.preferredWorkingHourPerDay) {
      return <React.Fragment>not defined</React.Fragment>;
    }
    return (
      <React.Fragment>
        {(
          "0" + Math.floor(this.state.user.preferredWorkingHourPerDay / 60)
        ).slice(-2) +
          ":" +
          ("0" + (this.state.user.preferredWorkingHourPerDay % 60)).slice(-2)}
      </React.Fragment>
    );
  };
  componentDidMount() {
    this.props
      .getProfile()
      .then(user => {
        this.setState({ user: user, busy: false });
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  }

  render() {
    if (this.state.busy) {
      return <LoadingIndicator />;
    }
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-2 col-sm-2 offset-md-5 text-center">
                    {this.state.user.image ? (
                      <img
                        className="rounded-circle"
                        src={"data:image/jpg;base64," + this.state.user.image}
                        alt={this.state.user.name}
                        width="150"
                        height="150"
                      />
                    ) : (
                      <img
                        className="rounded-circle"
                        src="https://via.placeholder.com/150/"
                        width="150"
                        height="150"
                        alt={this.state.user.name}
                      />
                    )}
                  </div>
                  <div className="col-md-8 offset-md-2 mt-3 text-center">
                    <div>
                      <h2>{this.state.user.name}</h2>
                      <hr />
                      <dl>
                        <dt>Email</dt>
                        <dd>{this.state.user.email}</dd>
                        <dt>Hours per day</dt>
                        <dd>{this.createHoursPerDay()}</dd>
                      </dl>
                    </div>
                  </div>
                  <hr className="my-4" />
                  <div className="col-sm-4 offset-sm-4">
                    <Link
                      to={`/profile/edit`}
                      className="btn btn-primary btn-block mb-2"
                    >
                      <i className="fas fa-edit" />
                      Edit
                    </Link>
                  </div>
                  <hr className="my-4" />
                  {this.state.user.provider == "local" && (
                    <div className="col-sm-4 offset-sm-4">
                      <Link
                        to={`/password/new`}
                        className="btn btn-primary btn-block mb-2"
                      >
                        <i className="fas fa-edit" />
                        Change password
                      </Link>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  getProfile: () => dispatch(getProfile())
});

export default connect(
  undefined,
  mapDispatchToProps
)(ProfilePage);
