import React from "react";
import { connect } from "react-redux";
import { setStartDate, setEndDate } from "../../../actions/taskfilters";
import { DateRangePicker } from "react-dates";

export class TaskFilter extends React.Component {
  state = {
    calendarFocused: null
  };
  onDatesChange = ({ startDate, endDate }) => {
    this.props.setStartDate(startDate);
    this.props.setEndDate(endDate);
  };
  onFocusChange = calendarFocused => {
    this.setState({ calendarFocused });
  };
  render() {
    return (
      <div className="col text-center">
        <DateRangePicker
          startDateId={"startDateId"}
          endDateId={"endDateId"}
          startDate={this.props.filters.startDate}
          endDate={this.props.filters.endDate}
          onDatesChange={this.onDatesChange}
          focusedInput={this.state.calendarFocused}
          onFocusChange={this.onFocusChange}
          showClearDates={true}
          numberOfMonths={1}
          isOutsideRange={() => false}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filters: state.taskfilters
});

const mapDispacthToProps = dispatch => ({
  setStartDate: startDate => dispatch(setStartDate(startDate)),
  setEndDate: endDate => dispatch(setEndDate(endDate))
});

export default connect(
  mapStateToProps,
  mapDispacthToProps
)(TaskFilter);
