import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import Workbook from "react-excel-workbook";
import ReactPaginate from "react-paginate";
import Alert from "react-s-alert";
import { Link } from "react-router-dom";
import { startGetTasks, startEditTask } from "../../../actions/task";
import LoadingIndicator from "../../../common/LoadingIndicator";
import TaskList from "../../../common/TaskList";
import TaskFilter from "./TaskFilter";

export class TasksPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      busy: false,
      page: 0
    };
  }

  renderList = () => {
    const filter = {
      page: this.state.page,
      startDate: this.props.startDate,
      endDate: this.props.endDate
    };
    this.props.getTasks(filter).then(
      result => {
        console.log("Success", result);
      },
      error => {
        console.log("Error getting tasks", error);
      }
    );
  };

  handlePageClick = data => {
    let page = data.selected;
    this.setState({ page: page }, () => {
      this.renderList();
    });
  };
  handleOnFinish = (id, request) => {
    this.props
      .editUserTask(id, request)
      .then(message => {
        Alert.success(message);
        this.renderList();
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };

  handleOnEdit = () => {
    console.log("Edited so refresh page");
    this.renderList();
  };

  componentDidMount() {
    this.renderList();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.endDate !== this.props.endDate ||
      prevProps.startDate !== this.props.startDate
    ) {
      this.renderList();
    }
  }

  render() {
    return (
      <div className="col">
        {this.props.busy && <LoadingIndicator />}
        {!this.props.busy && (
          <React.Fragment>
            <div className="jumbotron text-center">
              <h1 className="display-4 mb-2 mt-2 ">My Tasks</h1>
              <hr className="my-4" />
              <div className="row">
                <div className="col-md-4 offset-md-4 text-center">
                  <Link
                    to="/tasks/new"
                    className="btn btn-primary btn-block btn-lg"
                  >
                    <i class="fas fa-plus" /> New task
                  </Link>
                </div>
                <div className="col-md-4 offset-md-4 text-center mt-1">
                  <Workbook
                    filename="tasks.xlsx"
                    element={
                      <button className="btn btn-success btn-block btn-lg">
                        <i class="fas fa-file-excel" /> Export
                      </button>
                    }
                  >
                    <Workbook.Sheet data={this.props.tasks} name="Tasks">
                      <Workbook.Column
                        label="Date"
                        value={row =>
                          moment(row.startDate).format("YYYY.MM.DD")
                        }
                      />
                      <Workbook.Column
                        label="Total time"
                        value={row =>
                          row && row.duration
                            ? Math.floor(row.duration / 60) +
                              ":" +
                              (row.duration % 60)
                            : "On going"
                        }
                      />
                      <Workbook.Column label="Notes" value="description" />
                    </Workbook.Sheet>
                  </Workbook>
                </div>
              </div>
              <div className="row mt-4">
                <TaskFilter />
              </div>
            </div>

            <TaskList
              tasks={this.props.tasks}
              onEdit={this.handleOnEdit}
              onFinish={this.handleOnFinish}
            />
            {this.props.totalPages > 1 && (
              <ReactPaginate
                previousLabel={"Previous"}
                nextLabel={"Next"}
                breakLabel={"..."}
                forcePage={this.state.page}
                pageCount={this.props.totalPages}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={this.handlePageClick}
                containerClassName={"pagination justify-content-center"}
                subContainerClassName={"page-item"}
                activeClassName={"page-item active"}
                nextClassName={"page-item"}
                previousClassName={"page-item"}
                pageLinkClassName={"page-link"}
                nextLinkClassName={"page-link"}
                previousLinkClassName={"page-link"}
              />
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    busy: state.tasks.busy,
    tasks: state.tasks.list,
    totalPages: state.tasks.totalPages,
    startDate: state.taskfilters.startDate,
    endDate: state.taskfilters.endDate
  };
};

const mapDispatchToProps = dispatch => ({
  getTasks: filter => dispatch(startGetTasks(filter)),
  editUserTask: (taskId, request) => dispatch(startEditTask(taskId, request))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TasksPage);
