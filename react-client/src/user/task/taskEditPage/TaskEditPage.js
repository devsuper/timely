import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import TaskForm from "../../common/TaskForm";
import LoadingIndicator from "../../../common/LoadingIndicator";
import {
  startGetTask,
  startEditTask,
  startRemoveTask
} from "../../../actions/task";

export class TaskEditPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      taskId: props.id,
      busy: true,
      task: undefined
    };
  }
  componentDidMount() {
    this.props
      .getTask(this.state.taskId)
      .then(task => {
        this.setState({ task: task, busy: false });
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  }
  onSubmit = task => {
    this.props
      .editTask(this.state.taskId, task)
      .then(message => {
        Alert.success(message);
        this.props.history.push("/tasks");
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  onRemove = () => {
    this.props
      .removeTask(this.state.taskId)
      .then(message => {
        Alert.success(message);
        this.props.history.push("/tasks");
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    if (this.state.busy || this.props.busy) {
      return <LoadingIndicator />;
    }
    return (
      <div className="row justify-content-center">
        <div class="col-10">
          <div class="card">
            <h1 class="card-header text-center">Edit Task</h1>
            <div class="card-body">
              <div className="row">
                <div className="col-md-8 offset-md-2">
                  <TaskForm
                    task={this.state.task}
                    onSubmit={this.onSubmit}
                    onRemove={this.onRemove}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => ({
  id: props.match.params.id,
  busy: state.tasks.busy
});
const mapDispatchToProps = (dispatch, props) => ({
  getTask: id => dispatch(startGetTask(id)),
  editTask: (id, task) => dispatch(startEditTask(id, task)),
  removeTask: id => dispatch(startRemoveTask(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskEditPage);
