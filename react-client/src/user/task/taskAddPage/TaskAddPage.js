import React from "react";
import { connect } from "react-redux";
import Alert from "react-s-alert";
import TaskForm from "../../common/TaskForm";
import LoadingIndicator from "../../../common/LoadingIndicator";
import { startAddTask } from "../../../actions/task";

export class TaskAddPage extends React.Component {
  onSubmit = task => {
    this.props
      .addTask(task)
      .then(message => {
        Alert.success(message);
        this.props.history.push("/tasks");
      })
      .catch(error => {
        Alert.error(
          (error && error.message) ||
            "Oops! Something went wrong. Please try again!"
        );
      });
  };
  render() {
    if (this.props.busy) {
      return <LoadingIndicator />;
    }
    return (
      <React.Fragment>
        <div className="row justify-content-center">
          <div className="col-10">
            <div className="card">
              <h1 className="card-header text-center">New Task</h1>
              <div className="card-body">
                <div className="row">
                  <div className="col-md-8 offset-md-2">
                    <TaskForm onSubmit={this.onSubmit} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, props) => ({
  busy: state.tasks.busy
});

const mapDispatchToProps = dispatch => ({
  addTask: task => dispatch(startAddTask(task))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskAddPage);
