import React, { Component } from "react";
import "./Home.css";

class Home extends Component {
  render() {
    return (
      <div class="jumbotron jumbotron-fluid text-center">
        <div class="container">
          <h1 class="display-4">Timely</h1>
          <blockquote class="blockquote text-center">
            <p class="mb-0">
              Most creative people fall into one of two categories - either
              they're task-oriented, or they're time-oriented.
            </p>
            <footer class="blockquote-footer">by Brian Stelfreeze</footer>
          </blockquote>
        </div>
      </div>
    );
  }
}

export default Home;
