import React from "react";
import moment from "../../__mocks__/moment";
import { shallow } from "../../enzyme";

import TaskForm from "../../../user/common/TaskForm";

describe("TaskForm tests", () => {
  it("load form", () => {
    const wrapper = shallow(<TaskForm />);
    expect(wrapper.state("description")).toBe("");
  });
  it("load form with data", () => {
    let task = {
      description: "test",
      startDate: moment()
    };
    const wrapper = shallow(<TaskForm task={task} />);
    expect(wrapper.state("description")).toBe("test");
  });
  test("should set description on input change ", () => {
    const value = "test description";
    const wrapper = shallow(<TaskForm />);
    wrapper
      .find("input")
      .at(0)
      .simulate("change", { target: { value: value, name: "description" } });
    expect(wrapper.state("description")).toBe(value);
  });

  test("should call onSubmit prop for valid form submission", () => {
    const onSubmitSpy = jest.fn();
    let task = {
      description: "test",
      startDate: moment().toDate()
    };
    const wrapper = shallow(<TaskForm task={task} onSubmit={onSubmitSpy} />);
    wrapper.find("form").simulate("submit", { preventDefault: () => {} });
    expect(onSubmitSpy).toHaveBeenCalled();
  });
});
