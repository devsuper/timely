import React from "react";
import moment from "../../__mocks__/moment";
import { shallow } from "../../enzyme";
import { TaskItem } from "../../../common/TaskItem";

test("should render ExpenseList with expense", () => {
  const task = {
    id: 1,
    description: "test",
    startDate: moment()
  };
  const wrapper = shallow(<TaskItem key={task.id} task={task} />);
  expect(wrapper).toMatchSnapshot();
});
