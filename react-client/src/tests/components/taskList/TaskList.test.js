import React from "react";
import { TaskList } from "../../../common/TaskList";
import moment from "../../__mocks__/moment";
import { shallow } from "../../enzyme";

test("should render TaskList with expenses", () => {
  const tasks = [
    {
      id: 1,
      description: "test",
      startDate: moment()
    },
    {
      id: 2,
      description: "test",
      startDate: moment()
    },
    {
      id: 3,
      description: "test",
      startDate: moment()
    },
    {
      id: 4,
      description: "test",
      startDate: moment()
    }
  ];
  const wrapper = shallow(<TaskList tasks={tasks} />);
  expect(wrapper).toMatchSnapshot();
});

test("should render TaskList with empty message", () => {
  const wrapper = shallow(<TaskList tasks={[]} />);
  expect(wrapper).toMatchSnapshot();
});
