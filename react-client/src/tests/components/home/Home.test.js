import React from "react";
import { shallow } from "../../enzyme";
import Home from "../../../home/Home";

describe("Home tests", () => {
  it("renders home page", () => {
    const wrapper = shallow(<Home />);
    expect(wrapper.find(".home-title")).toBeDefined();
  });
});
