import { authConstants, startLogout } from "../actions/auth";

export const request = options => {
  const headers = new Headers({
    "Content-Type": "application/json"
  });

  if (localStorage.getItem(authConstants.ACCESS_TOKEN)) {
    headers.append(
      "Authorization",
      "Bearer " + localStorage.getItem(authConstants.ACCESS_TOKEN)
    );
  }

  const defaults = { headers: headers };
  options = Object.assign({}, defaults, options);

  return fetch(options.url, options).then(response => handleResponse(response));
};

export const requestNoHeader = options => {
  const headers = new Headers();
  if (localStorage.getItem(authConstants.ACCESS_TOKEN)) {
    headers.append(
      "Authorization",
      "Bearer " + localStorage.getItem(authConstants.ACCESS_TOKEN)
    );
  }

  const defaults = { headers: headers };
  options = Object.assign({}, defaults, options);

  return fetch(options.url, options).then(response => handleResponse(response));
};

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    console.log("Response", response);
    if (response.status === 401) {
      startLogout();
    }
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
