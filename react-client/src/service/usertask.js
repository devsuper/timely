import { request } from "./utils";

export const userstasks = {
  get,
  getAll,
  create,
  update,
  remove
};

export function getAll(userId, filter) {
  const params = {
    page: filter.page
  };
  if (filter.startDate) {
    params.startDate = filter.startDate.format();
  }
  if (filter.endDate) {
    params.endDate = filter.endDate.format();
  }
  const urlParams = new URLSearchParams(Object.entries(params));

  return request({
    url: `/user/${userId}/task?${urlParams}`,
    method: "GET"
  });
}

export function get(userId, id) {
  return request({
    url: `/user/${userId}/task/${id}`,
    method: "GET"
  });
}

export function create(userId, task) {
  return request({
    url: `/user/${userId}/task/`,
    method: "POST",
    body: JSON.stringify(task)
  });
}

export function update(userId, taskId, task) {
  return request({
    url: `/user/${userId}/task/${taskId}`,
    method: "PUT",
    body: JSON.stringify(task)
  });
}

export function remove(userId, id) {
  return request({
    url: `/user/${userId}/task/${id}`,
    method: "DELETE"
  });
}
