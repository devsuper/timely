import { request, requestNoHeader } from "./utils";

export const users = {
  get,
  getAll,
  create,
  update,
  remove,
  invite,
  uploadAvatar,
  resetPassword,
  changePassword
};

export function get(id) {
  return request({
    url: `/user/${id}`,
    method: "GET"
  });
}

export function resetPassword(userId) {
  return request({
    url: `/user/reset/${userId}`,
    method: "GET"
  });
}

export function changePassword(password) {
  return request({
    url: `/user/password/`,
    method: "PUT",
    body: JSON.stringify({ password })
  });
}

export function uploadAvatar(userId, avatar) {
  if (!avatar) return Promise.resolve("");
  let data = new FormData();
  data.append("file", avatar);
  data.append("name", avatar.name);

  console.log("Name, ", avatar.name);

  return requestNoHeader({
    url: `/user/avatar/upload/${userId}`,
    method: "POST",
    body: data
  });
}

export function invite(email) {
  const params = {
    email
  };
  const urlParams = new URLSearchParams(Object.entries(params));

  return request({
    url: `/user/invite?${urlParams}`,
    method: "GET"
  });
}

export function getAll() {
  return request({
    url: `/user/`,
    method: "GET"
  });
}

export function create(user) {
  return request({
    url: `/user/`,
    method: "POST",
    body: JSON.stringify(user)
  });
}

export function update(id, user) {
  return request({
    url: `/user/${id}`,
    method: "PUT",
    body: JSON.stringify(user)
  });
}

export function remove(id) {
  return request({
    url: `/user/${id}`,
    method: "DELETE"
  });
}
