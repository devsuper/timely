import { authConstants } from "../actions/auth";
import { request } from "./utils";

export const auth = {
  login,
  signup,
  getCurrentUser,
  updateProfile
};

export function getCurrentUser() {
  if (!localStorage.getItem(authConstants.ACCESS_TOKEN)) {
    return Promise.reject("No access token set.");
  }

  return request({
    url: "/user/me",
    method: "GET"
  });
}

export function login(loginRequest) {
  return request({
    url: "/auth/login",
    method: "POST",
    body: JSON.stringify(loginRequest)
  });
}

export function signup(signupRequest) {
  return request({
    url: "/auth/signup",
    method: "POST",
    body: JSON.stringify(signupRequest)
  });
}

export function updateProfile(user) {
  return request({
    url: `/profile`,
    method: "PUT",
    body: JSON.stringify(user)
  });
}
