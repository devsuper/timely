import { request } from "./utils";

export const task = {
  getAll,
  get,
  create,
  update,
  remove
};

export function getAll(filter) {
  const params = {
    page: filter.page,
    sort: "startDate,desc"
  };
  if (filter.startDate) {
    params.startDate = filter.startDate.format();
  }
  if (filter.endDate) {
    params.endDate = filter.endDate.format();
  }
  const urlParams = new URLSearchParams(Object.entries(params));
  return request({
    url: `/task?${urlParams}`,
    method: "GET"
  });
}

export function create(task) {
  return request({
    url: `/task/`,
    method: "POST",
    body: JSON.stringify(task)
  });
}

export function update(id, task) {
  return request({
    url: `/task/${id}`,
    method: "PUT",
    body: JSON.stringify(task)
  });
}

export function get(id) {
  return request({
    url: `/task/${id}`,
    method: "GET"
  });
}

export function remove(id) {
  return request({
    url: `/task/${id}`,
    method: "DELETE"
  });
}
