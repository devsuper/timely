export const hasRole = (userRoles, roles) =>
  roles.some(role => userRoles.includes(role));
