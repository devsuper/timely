import React, { Component } from "react";
import { Router, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";
import createHistory from "history/createBrowserHistory";
import Home from "../home/Home";
import Login from "../user/login/Login";
import Signup from "../user/signup/Signup";
import TasksPage from "../user/task/tasksPage/TasksPage";
import OAuth2RedirectHandler from "../user/oauth2/OAuth2RedirectHandler";
import NotFound from "../common/NotFound";
import LoadingIndicator from "../common/LoadingIndicator";
import { startGetProfile } from "../actions/auth";
import PrivateRoute from "../common/PrivateRoute";
import ManagerRoute from "../common/ManagerRoute";
import AdminRoute from "../common/AdminRoute";
import PublicRoute from "../common/PublicRoute";
import "react-s-alert/dist/s-alert-default.css";
import "react-s-alert/dist/s-alert-css-effects/slide.css";
import { hasRole } from "../selectors/auth";
import "./App.css";
import Alert from "react-s-alert";
import TaskAddPage from "../user/task/taskAddPage/TaskAddPage";
import TaskEditPage from "../user/task/taskEditPage/TaskEditPage";
import UsersPage from "../user/manager/usersPage/UsersPage";
import UserAddPage from "../user/manager/userAddPage/UserAddPage";
import UserEditPage from "../user/manager/userEditPage/UserEditPage";
import UserTasksPage from "../user/manager/userTasks/UserTasksPage";
import UserAddTaskPage from "../user/manager/userAddTaskPage/UserAddTaskPage";
import UserEditTaskPage from "../user/manager/userEditTaskPage/UserEditTaskPage";
import ProfilePage from "../user/profile/ProfilePage";
import EditProfilePage from "../user/ediProfilePage/EditProfilePage";
import InvitePage from "../user/manager/invitePage/InvitePage";
import ChangePasswordPage from "../user/manager/changePassword/ChangePasswordPage";

export const history = createHistory();

class App extends Component {
  componentDidMount() {
    this.props.getProfile();
  }

  render() {
    if (this.props.busy) {
      return <LoadingIndicator />;
    }

    return (
      <React.Fragment>
        <Router history={history}>
          <Switch>
            <PublicRoute path="/" exact={true} component={Home} />
            <PublicRoute path="/login" exact={true} component={Login} />
            <PublicRoute path="/signup" exact={true} component={Signup} />
            <PublicRoute
              path="/oauth2/redirect"
              component={OAuth2RedirectHandler}
            />
            <PrivateRoute path="/tasks" exact={true} component={TasksPage} />
            <PrivateRoute
              path="/tasks/new"
              exact={true}
              component={TaskAddPage}
            />
            <PrivateRoute
              path="/tasks/edit/:id"
              exact={true}
              component={TaskEditPage}
            />
            <ManagerRoute path="/users" exact={true} component={UsersPage} />
            <ManagerRoute
              path="/users/new"
              exact={true}
              component={UserAddPage}
            />
            <ManagerRoute
              path="/users/edit/:id"
              exact={true}
              component={UserEditPage}
            />
            <ManagerRoute
              path="/users/invite"
              exact={true}
              component={InvitePage}
            />
            <AdminRoute
              path="/users/:id/tasks"
              exact={true}
              component={UserTasksPage}
            />
            <AdminRoute
              path="/users/:id/tasks/add"
              exact={true}
              component={UserAddTaskPage}
            />
            <AdminRoute
              path="/users/:id/tasks/edit/:taskId"
              exact={true}
              component={UserEditTaskPage}
            />
            <PrivateRoute
              path="/profile"
              exact={true}
              component={ProfilePage}
            />
            <PrivateRoute
              path="/profile/edit"
              exact={true}
              component={EditProfilePage}
            />
            <PrivateRoute
              path="/password/new"
              exact={true}
              component={ChangePasswordPage}
            />

            <Route path="/oauth2/redirect" component={OAuth2RedirectHandler} />
            <PublicRoute component={NotFound} />
          </Switch>
        </Router>
        <Alert
          stack={{ limit: 3 }}
          timeout={3000}
          position="top-right"
          effect="slide"
          offset={65}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  busy: !!state.auth.busy,
  authenticated: !!state.auth.uid,
  isUser: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_USER"]),
  isAdmin: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_ADMIN"]),
  isManager: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_MANAGER"])
});

const mapDispatchToProps = dispatch => ({
  getProfile: () => dispatch(startGetProfile())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
