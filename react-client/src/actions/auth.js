import { auth } from "../service/auth";
import { users } from "../service/users";

export const authConstants = {
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  LOGOUT: "LOGOUT",
  GET_PROFILE_SUCCESS: "GET_PROFILE_SUCCESS",
  EDIT_PROFILE_SUCCESS: "EDIT_PROFILE_SUCCESS",
  AUTH_BUSY: "AUTH_BUSY",
  ACCESS_TOKEN: "accessToken",
  ACCESS_ROLES: "accessRoles"
};

const busy = busy => ({
  type: authConstants.AUTH_BUSY,
  busy
});

export const login = (uid, role) => ({
  type: authConstants.LOGIN_SUCCESS,
  uid,
  role
});

export const startLogin = loginRequest => {
  return dispatch => {
    dispatch(busy(true));
    return auth.login(loginRequest).then(
      user => {
        dispatch(busy(false));
        localStorage.setItem(authConstants.ACCESS_TOKEN, user.accessToken);
        localStorage.setItem(authConstants.ACCESS_ROLES, user.role);
        dispatch(login(user.accessToken, user.role));
        dispatch(startGetProfile());
        return "Login with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

const logout = () => ({
  type: authConstants.LOGOUT
});

export const startLogout = () => {
  localStorage.removeItem(authConstants.ACCESS_TOKEN);
  localStorage.removeItem(authConstants.ACCESS_ROLES);
  return logout();
};

const getProfileSuccess = profile => ({
  type: authConstants.GET_PROFILE_SUCCESS,
  profile
});

export const getProfile = () => {
  return dispatch => {
    return auth.getCurrentUser();
  };
};

export const startGetProfile = () => {
  return dispatch => {
    dispatch(busy(true));
    return auth.getCurrentUser().then(
      profile => {
        dispatch(getProfileSuccess(profile));
        return profile;
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

const editProfile = profile => ({
  type: authConstants.EDIT_PROFILE_SUCCESS,
  profile
});

export const startEditProfile = (profile, avatar) => {
  return dispatch => {
    dispatch(busy(true));
    return users.uploadAvatar(profile.id, avatar).then(() => {
      return auth.updateProfile(profile).then(
        () => {
          dispatch(busy(false));
          dispatch(editProfile(profile));
          return "Profile updated with success!";
        },
        error => {
          dispatch(busy(false));
          return Promise.reject({
            message: error
          });
        }
      );
    });
  };
};

export const startSignUpRequst = signupRequest => {
  return auth.signup(signupRequest).then(
    () => {
      return Promise.resolve({ message: "User created with success!" });
    },
    error => {
      return Promise.reject({
        message: error
      });
    }
  );
};
