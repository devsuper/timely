import { task } from "../service/task";

export const taskConstants = {
  SET_TASKS_SUCCESS: "SET_TASKS_SUCCESS",
  SET_TASK_SUCCESS: "SET_TASK_SUCCESS",
  TASK_BUSY: "TASK_BUSY"
};

const busy = busy => ({
  type: taskConstants.TASK_BUSY,
  busy
});

const setTasksSuccess = (tasks, totalPages) => ({
  type: taskConstants.SET_TASKS_SUCCESS,
  list: tasks,
  totalPages: totalPages
});

export const startGetTask = id => {
  return dispatch => {
    return task.get(id);
  };
};

export const startGetTasks = filer => {
  return dispatch => {
    dispatch(busy(true));
    return task.getAll(filer).then(
      tasks => {
        console.log("TASKS", tasks);
        dispatch(setTasksSuccess(tasks.content, tasks.totalPages));
        return "Success";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startAddTask = taskRequest => {
  return dispatch => {
    dispatch(busy(true));
    return task.create(taskRequest).then(
      () => {
        dispatch(busy(false));
        return "Task created with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startEditTask = (id, taskRequest) => {
  return dispatch => {
    dispatch(busy(true));
    return task.update(id, taskRequest).then(
      () => {
        dispatch(busy(false));
        return "Task edited with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startRemoveTask = id => {
  return dispatch => {
    dispatch(busy(true));
    return task.remove(id).then(
      () => {
        dispatch(busy(false));
        return "Task delete with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};
