export const tasksFiltersConstants = {
  SET_START_DATE: "SET_START_DATE",
  SET_END_DATE: "SET_END_DATE"
};

export const setStartDate = startDate => ({
  type: tasksFiltersConstants.SET_START_DATE,
  startDate
});

export const setEndDate = endDate => ({
  type: tasksFiltersConstants.SET_END_DATE,
  endDate
});
