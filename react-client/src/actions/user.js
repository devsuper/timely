import { users } from "../service/users";

export const usersConstants = {
  SET_USERS_SUCCESS: "SET_USERS_SUCCESS",
  USER_BUSY: "USER_BUSY"
};

const busy = busy => ({
  type: usersConstants.USER_BUSY,
  busy
});

const setUsersSuccess = (users, totalPages) => ({
  type: usersConstants.SET_USERS_SUCCESS,
  list: users,
  totalPages: totalPages
});

export const startGetUser = id => {
  return dispatch => {
    return users.get(id);
  };
};

export const resetPassword = userId => {
  return dispatch => {
    dispatch(busy(true));
    return users.resetPassword(userId).then(
      () => {
        dispatch(busy(false));
        return "User password reseted with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startInvite = email => {
  return dispatch => {
    dispatch(busy(true));
    return users.invite(email).then(
      () => {
        dispatch(busy(false));
        return "User invited with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startChangePassword = password => {
  return dispatch => {
    dispatch(busy(true));
    return users.changePassword(password).then(
      () => {
        dispatch(busy(false));
        return "Password changed with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startGetUsers = () => {
  return dispatch => {
    dispatch(busy(true));
    return users.getAll().then(
      users => {
        dispatch(setUsersSuccess(users.content, users.totalPages));
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startAddUser = (user, avatar) => {
  return dispatch => {
    dispatch(busy(true));
    return users.create(user).then(
      user => {
        return users.uploadAvatar(user.id, avatar).then(() => {
          dispatch(busy(false));
          return "User created with success!";
        });
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};

export const startEditUser = (id, user, avatar) => {
  return dispatch => {
    dispatch(busy(true));
    return users.uploadAvatar(id, avatar).then(() => {
      users.update(id, user).then(
        () => {
          dispatch(busy(false));
          return "User edited with success!";
        },
        error => {
          dispatch(busy(false));
          return Promise.reject({
            message: error
          });
        }
      );
    });
  };
};

export const startRemoveUser = id => {
  return dispatch => {
    dispatch(busy(true));
    return users.remove(id).then(
      () => {
        dispatch(busy(false));
        dispatch(startGetUsers());
        return "User delete with success!";
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          message: error
        });
      }
    );
  };
};
