import { userstasks } from "../service/usertask";

export const userTasksConstants = {
  SET_USERS_TASKS_SUCCESS: "SET_USERS_TASKS_SUCCESS",
  USER_TASKS_BUSY: "USER_TASKS_BUSY"
};

const busy = busy => ({
  type: userTasksConstants.USER_TASKS_BUSY,
  busy
});

const setUserTasksSuccess = (tasks, totalPages) => ({
  type: userTasksConstants.SET_USERS_TASKS_SUCCESS,
  list: tasks,
  totalPages: totalPages
});

export const startGetTask = (userId, id) => {
  return dispatch => {
    return userstasks.get(userId, id);
  };
};

export const startGetUserTasks = (userId, filter) => {
  return dispatch => {
    dispatch(busy(true));
    return userstasks.getAll(userId, filter).then(
      tasks => {
        dispatch(busy(false));
        dispatch(setUserTasksSuccess(tasks.content, tasks.totalPages));
      },
      error => {
        dispatch(busy(false));
        return Promise.reject({
          errorMsg: "error",
          error: new Error("Error getting task")
        });
      }
    );
  };
};

export const startAddUserTask = (userId, task) => {
  return dispatch => {
    dispatch(busy(true));
    return userstasks.create(userId, task).then(
      () => {
        dispatch(busy(false));
        return "Task created with success!";
      },
      error => {
        dispatch(busy(false));
        throw Object.assign(new Error("Error creating task"), {
          error: error
        });
      }
    );
  };
};

export const startEditUserTask = (userId, id, task) => {
  return dispatch => {
    dispatch(busy(true));
    return userstasks.update(userId, id, task).then(
      () => {
        dispatch(busy(false));
        return "Task edited with success!";
      },
      error => {
        dispatch(busy(false));
        throw Object.assign(new Error("Error editing task"), {
          error: error
        });
      }
    );
  };
};

export const startRemoveUserTask = (userId, id) => {
  return dispatch => {
    dispatch(busy(true));
    return userstasks.remove(userId, id).then(
      () => {
        dispatch(busy(false));
        return "Task delete with success!";
      },
      error => {
        dispatch(busy(false));
        throw Object.assign(new Error("Error editing task"), {
          error: error
        });
      }
    );
  };
};
