import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Link } from "react-router-dom";
import { startEditTask } from "../actions/task";
import "./TaskItem.css";

export class TaskItem extends Component {
  handleOnFinish = e => {
    e.preventDefault();
    let request = {
      description: this.props.task.description,
      startDate: this.props.task.startDate,
      endDate: moment()
    };
    this.props.onFinish(this.props.task.id, request);
  };
  render() {
    return (
      <div
        id={this.props.task.id}
        className={
          this.props.task.exceedDailyHour
            ? "card border-danger mb-3"
            : "card border-success mb-3"
        }
      >
        <div className="card-body">
          <div className="row">
            <div className="col-md-7 offset-md-1 tags p-b-2">
              <h3 className="d-inline-block">{this.props.task.description}</h3>{" "}
              {!this.props.task.endDate && (
                <span class="badge badge-warning align-top">ongoing</span>
              )}
              <p>
                {moment(this.props.task.startDate).format("LLLL")} ->{" "}
                {this.props.task.endDate && (
                  <React.Fragment>
                    {moment(this.props.task.endDate).format("LLLL")}
                  </React.Fragment>
                )}
              </p>
              {this.props.task.endDate && (
                <p>
                  <b>Duration: </b>{" "}
                  {("0" + Math.floor(this.props.task.duration / 60)).slice(-2) +
                    ":" +
                    ("0" + (this.props.task.duration % 60)).slice(-2)}
                </p>
              )}
            </div>
            <div className="col-md-2 offset-md-1">
              <p>
                <Link
                  to={`tasks/edit/${this.props.task.id}`}
                  className="btn btn-primary btn-block"
                >
                  <i class="fas fa-edit" />
                  Edit
                </Link>
              </p>
              {!this.props.task.endDate && (
                <p>
                  <button
                    type="button"
                    className="btn btn-success btn-block"
                    onClick={this.handleOnFinish}
                    intent="Danger"
                  >
                    <i class="fas fa-stopwatch" /> End task
                  </button>
                </p>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  editTask: (id, task) => dispatch(startEditTask(id, task))
});

export default connect(
  undefined,
  mapDispatchToProps
)(TaskItem);
