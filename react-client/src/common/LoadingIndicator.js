import React from "react";
import { Spinner } from "@blueprintjs/core";

export default function LoadingIndicator(props) {
  return (
    <div
      className="loading-indicator"
      style={{ display: "block", textAlign: "center", marginTop: "30px" }}
    >
      <Spinner />
    </div>
  );
}
