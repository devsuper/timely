import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { authConstants } from "../actions/auth";
import { hasRole } from "../selectors/auth";
import AppHeader from "./AppHeader";

export const AdminRoute = ({
  isAuthenticated,
  isAdmin,
  isManager,
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    component={props =>
      isAuthenticated && isAdmin ? (
        <React.Fragment>
          <AppHeader />
          <div className="container mt-3 mb-3">
            <Component {...props} />
          </div>
        </React.Fragment>
      ) : (
        <Redirect to="/" />
      )
    }
  />
);

const mapStateToProps = state => ({
  isAuthenticated: !!(localStorage.getItem(authConstants.ACCESS_TOKEN) != null),
  isAdmin: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_ADMIN"])
});

export default connect(mapStateToProps)(AdminRoute);
