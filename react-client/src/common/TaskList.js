import React from "react";
import TaskItem from "./TaskItem";

export const TaskList = props => (
  <React.Fragment>
    {props.tasks.length === 0 ? (
      <div className="row mt-4">
        <div className="col">
          <p className="text-center">No tasks</p>
        </div>
      </div>
    ) : (
      props.tasks.map(task => {
        return (
          <TaskItem
            key={task.id}
            task={task}
            onEdit={props.onEdit}
            onFinish={props.onFinish}
          />
        );
      })
    )}
  </React.Fragment>
);
export default TaskList;
