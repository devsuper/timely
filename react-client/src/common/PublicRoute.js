import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { authConstants } from "../actions/auth";
import AppHeader from "../common/AppHeader";

export const PublicRoute = ({
  isAuthenticated,
  component: Component,
  ...rest
}) => (
  <Route
    {...rest}
    component={props =>
      isAuthenticated ? (
        <Redirect to="/tasks" />
      ) : (
        <React.Fragment>
          <AppHeader />
          <div className="container mt-3 mb-3">
            <Component {...props} />
          </div>
        </React.Fragment>
      )
    }
  />
);

const mapStateToProps = state => ({
  isAuthenticated: !!(localStorage.getItem(authConstants.ACCESS_TOKEN) != null)
});

export default connect(mapStateToProps)(PublicRoute);
