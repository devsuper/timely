import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { authConstants } from "../actions/auth";
import { hasRole } from "../selectors/auth";
import { startLogout } from "../actions/auth";
import "./AppHeader.css";

class AppHeader extends Component {
  render() {
    const userHeader = (
      <li className="nav-item">
        <NavLink className="nav-link" exact={true} to="/tasks">
          My tasks
        </NavLink>
      </li>
    );
    const managerHeader = (
      <React.Fragment>
        <li className="nav-item">
          <NavLink className="nav-link" exact={true} to="/users">
            Users
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" exact={true} to="/users/new">
            New User
          </NavLink>
        </li>
      </React.Fragment>
    );
    const adminHeader = (
      <li className="nav-item">
        <NavLink className="nav-link" exact={true} to="/users/invite">
          Invite user
        </NavLink>
      </li>
    );

    return (
      <nav className="navbar navbar-expand-lg sticky-top  navbar-dark bg-primary">
        <div className="container">
          <Link to="/" className="navbar-brand">
            Timely
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            {this.props.isAuthenticated ? (
              <ul className="navbar-nav mr-auto">
                {userHeader}
                {(this.props.isManager || this.props.isAdmin) && managerHeader}
                {this.props.isAdmin && adminHeader}
              </ul>
            ) : (
              <React.Fragment>
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/login">
                      Login
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to="/signup">
                      Signup
                    </NavLink>
                  </li>
                </ul>
              </React.Fragment>
            )}
            {this.props.isAuthenticated && (
              <ul className="navbar-nav ml-auto">
                <NavLink className="nav-link" to="/profile">
                  Profile
                </NavLink>
                <li className="nav-item">
                  <a className="nav-link" onClick={this.props.logout}>
                    Logout
                  </a>
                </li>
              </ul>
            )}
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: !!(localStorage.getItem(authConstants.ACCESS_TOKEN) != null),
  isUser: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_USER"]),
  isAdmin: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_ADMIN"]),
  isManager: !!state.auth.uid && hasRole(state.auth.roles, ["ROLE_MANAGER"])
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(startLogout())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppHeader);
