import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import "./index.css";
import App from "./app/App";
import registerServiceWorker from "./registerServiceWorker";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "./style/input-moment.min.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";
import { authConstants } from "./actions/auth";
import { login } from "./actions/auth";

const store = configureStore();
const jsx = (
  <Provider store={store}>
    <App />
  </Provider>
);

const renderApp = () => {
  ReactDOM.render(jsx, document.getElementById("root"));
};

if (
  localStorage.getItem(authConstants.ACCESS_TOKEN) &&
  localStorage.getItem(authConstants.ACCESS_ROLES)
) {
  store.dispatch(
    login(
      localStorage.getItem(authConstants.ACCESS_TOKEN),
      localStorage.getItem(authConstants.ACCESS_ROLES)
    )
  );
  renderApp();
} else {
  renderApp();
}

registerServiceWorker();
