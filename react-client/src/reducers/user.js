import { usersConstants } from "../actions/user";

export default (state = { list: [], totalPages: 0, busy: false }, action) => {
  switch (action.type) {
    case usersConstants.USER_BUSY:
      return { ...state, busy: action.busy };
    case usersConstants.SET_USERS_SUCCESS:
      return {
        ...state,
        list: [...action.list],
        totalPages: action.totalPages,
        busy: false
      };
    default:
      return state;
  }
};
