import moment from "moment";
import { tasksFiltersConstants } from "../actions/taskfilters";

const filtersReducerDefaultState = {
  startDate: moment().startOf("month"),
  endDate: moment().endOf("month")
};

export default (state = filtersReducerDefaultState, action) => {
  switch (action.type) {
    case tasksFiltersConstants.SET_START_DATE:
      return {
        ...state,
        startDate: action.startDate
      };
    case tasksFiltersConstants.SET_END_DATE:
      return {
        ...state,
        endDate: action.endDate
      };
    default:
      return state;
  }
};
