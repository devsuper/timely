import { userTasksConstants } from "../actions/usertasks";

export default (state = { list: [], totalPages: 0, busy: false }, action) => {
  switch (action.type) {
    case userTasksConstants.USER_TASKS_BUSY:
      return { ...state, busy: action.busy };
    case userTasksConstants.SET_USERS_TASKS_SUCCESS:
      return {
        ...state,
        list: [...action.list],
        totalPages: action.totalPages,
        busy: false
      };
    default:
      return state;
  }
};
