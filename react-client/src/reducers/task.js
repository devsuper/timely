import { taskConstants } from "../actions/task";

export default (
  state = { list: [], task: undefined, totalPages: 0, busy: false },
  action
) => {
  switch (action.type) {
    case taskConstants.TASK_BUSY:
      return { ...state, busy: action.busy };
    case taskConstants.SET_TASKS_SUCCESS:
      return {
        ...state,
        list: [...action.list],
        totalPages: action.totalPages,
        busy: false
      };
    case taskConstants.SET_TASK_SUCCESS:
      return {
        ...state,
        task: action.task,
        busy: false
      };
    default:
      return state;
  }
};
