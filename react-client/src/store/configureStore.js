import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import authReducer from "../reducers/auth";
import taskReducer from "../reducers/task";
import taskFiltersReducer from "../reducers/taskfilters";
import userReducer from "../reducers/user";
import userTasksReducer from "../reducers/usertasks";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(
    combineReducers({
      auth: authReducer,
      tasks: taskReducer,
      taskfilters: taskFiltersReducer,
      users: userReducer,
      userstasks: userTasksReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
  );

  return store;
};
